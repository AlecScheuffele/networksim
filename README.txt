README:

This project was part of a self assigned, planned, and managed CPD project for Macquarie University's 2017 Software Engineering capstone unit.
The purpose of this network simulator was to facilitate my understanding of network/data-comm concepts, and to provide a visual demonstration of
these concepts to the rest of the class.


######################################################################################
######################################################################################

The following information concerns some of the related files:

--------------------------Network Simulator Source Code------------------------------
the network simulator consists of 3 python files:
    - netView.py        --- manages all GUI elements of the simulation
    - netSim.py         --- contains the simulation model, and its class structure
    - sampleNetworks.py --- contains ``scripts'' for building sample networks
-------------------------Network Simulator Project Report----------------------------
    - Portfolio         --- This is the final deliverable for the Unit.  Part 1 is UNRELATED.
                            Part 2 documents The learning/development of this project.  screen-
                            shots and selected snippets are located in appendix.


######################################################################################
######################################################################################


TO BE NOTED:
It should be noted that this application is FAR from complete outside the scope of the
CPD project.  There are many issues that are present...like:
    -Creating network components within the application itself (not using a script) is incomplete and buggy
    -TCP connections don't close (no FIN).
    -The Log viewer double-posts the first step when adjusting filters.
    -Step shortcut (spacebar) will enable/disable UI check-buttons if simulation canvas is not clicked first
    .....many more
Also the code itself needs some heavy refactoring.  (e.g. Network-Model Layers should be class-based)



######################################################################################
######################################################################################


USING THE APPLICATION:
    To load a simulation:
        Menu-bar: Sims   (ignore workspace)

    send message:
        Tab-bar: Commands

    resend last message:
        key: 's'

    step:
        key: spacebar  --- make sure that the simulation canvas is selected

--REMEMBER--- TO SELECT THE DESIRED CHECK-BUTTON FILTERS IN THE THE INSEPCTOR/LOG --- default will display nothing



######################################################################################
######################################################################################


VIEWING THE CODE:
Make sure your text editor has text-wrap disabled!

LAUNCHING THE APPLICATION:
From the project directory, run the command ``python netView.py''

DEPENDENCIES:
This application requires the Tkinter to be installed on your machine
Tkinter comes installed by default on MAC_OS.

