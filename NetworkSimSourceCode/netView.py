from Tkinter import*
import time
import networkSim as sim
import Queue
import ttk as ttk
import tkSimpleDialog
import sampleNetworks as sample
import math


#TODO: make log update itself.  only need step number for step history.  just keep checking log record for new data
#TODO: -- simulate network diagnostics by pingint --- ADD PING COMMAND
#TODO: update console output when "send message" is pressed.  make sure it doesnt re-print the current step
#TODO: --VIEW WINDOW--  view window function should be calculated at the end.  offset should NOT BE EXPLICIT FOR EVERY VARIABLE!!!!
#TODO: check weather prefix class has enough subnet space left before allocating----- not sure if i have done this :/ double-check
#TODO: restrict subnet prefix entry to valid


#-----------------------------------------------------------------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------------------------------------------------------------


tObjects = {}                                       #dictionary of all tkinter graphics objects. simID --> tNode -or- tLink
mousePos = [0, 0]
mousePressed = False
objectBeingDragged = None                           #the simID of the object being dragged (the one that will map to mousePos)
offset = [0.0, 0.0]                                  #the view offset (for scrolling the simulation canvas) --- see todo "VIEW WINDOW"
selector = None                                     #selection box coodinates --- [start corner, dragged corner]
dashOffset = 0                                      #as this value iterates at an appropriate modular interval, dashed lines appear to move/flow
zoom = 1.0
nodeWidth = 20
themes = {}

#TODO: flvefljvbeflkj

#-----------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------
#Workspace Object Classes
#-----------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------
class tNode(object):
    def __init__(self, node, centerPos):
        self.node = node                           #node object in networkSim model (router, hub, switch, client)
        self.centerPos = centerPos
        self.width = nodeWidth*zoom
        self.dragged = False
        self.selected = False

    def draw(self):
        #---positions---
        offsetCenterPos = [(self.centerPos[0] - offset[0])*zoom, (self.centerPos[1]-offset[1])*zoom]
        topLeft = [x-self.width/2 for x in offsetCenterPos]
        bottomRight = [x+self.width/2 for x in offsetCenterPos]
        #-----draw------
        if self.selected:
            canvas.create_oval(topLeft, bottomRight, fill=themes[themeVar.get()].nodeFill_highlight, tags="networkObjects", outline=themes[themeVar.get()].nodeOutline_highlight, width=3)
        else:
            canvas.create_oval(topLeft, bottomRight, fill=themes[themeVar.get()].nodeFill_normal, tags="networkObjects", outline=themes[themeVar.get()].nodeOutline_normal, width=2)
        global checkVarShowLabelsName
        if checkVarShowLabelsName.get() is 1:
            if checkVarFilterLabels.get() is 0 or self.selected:
                canvas.create_text([offsetCenterPos[0], offsetCenterPos[1]+(self.width-(self.width/5))], text=self.node.name, tags="networkObjects", fill=themes[themeVar.get()].label)


class tLink(object):
    def __init__(self, link, lineStart, lineEnd):
        self.link = link                           #link object in networkSim model
        self.lineStart = lineStart
        self.lineEnd = lineEnd

    def draw(self):
        #---key positions---
        offsetLineStart = [(self.lineStart[0] - offset[0])*zoom, (self.lineStart[1]-offset[1])*zoom]
        offsetLineEnd = [(self.lineEnd[0] - offset[0])*zoom, (self.lineEnd[1]-offset[1])*zoom]
        mag = math.sqrt((offsetLineEnd[0]-offsetLineStart[0])**2 + (offsetLineEnd[1]-offsetLineStart[1])**2)
        ArrowTipOffsetVector = ((offsetLineEnd[0]-offsetLineStart[0])/mag * ((nodeWidth/2)*zoom), (offsetLineEnd[1]-offsetLineStart[1])/mag * ((nodeWidth/2)*zoom))
        drawLineStart = (offsetLineStart[0] + ArrowTipOffsetVector[0], offsetLineStart[1] + ArrowTipOffsetVector[1])
        drawLineEnd = (offsetLineEnd[0] - ArrowTipOffsetVector[0], offsetLineEnd[1] - ArrowTipOffsetVector[1])
        center = [min(offsetLineStart[0], offsetLineEnd[0]) + (max(offsetLineStart[0], offsetLineEnd[0])-min(offsetLineStart[0], offsetLineEnd[0]))/2, min(offsetLineStart[1], offsetLineEnd[1]) + (max(offsetLineStart[1], offsetLineEnd[1])-min(offsetLineStart[1], offsetLineEnd[1]))/2]
        transferring = False
        #-----draw------
        #
        for message in sim.links[self.link].message_GoingTo_Point_A:
            canvas.create_line(drawLineStart, drawLineEnd, fill=themes[themeVar.get()].linkColor_transfer, width=4, tags="networkObjects", arrow=FIRST*checkVarArrowhead.get(), dash=(6, 10), dashoff=dashOffset)
            transferring = True
        for message in sim.links[self.link].message_GoingTo_Point_B:
            canvas.create_line(drawLineStart, drawLineEnd, fill=themes[themeVar.get()].linkColor_transfer, width=4, tags="networkObjects", arrow=LAST*checkVarArrowhead.get(), dash=(6, 10), dashoff=100-dashOffset)
            transferring = True
        if not(transferring):
            canvas.create_line(offsetLineStart, offsetLineEnd, tags="networkObjects", fill=themes[themeVar.get()].linkColor_idle, width=2)
        global checkVarShowLabelsLinks
        global checkVarFilterLabels
        if checkVarShowLabelsLinks.get() is 1:
            if checkVarFilterLabels.get() is 0 or ((tObjects[sim.links[self.link].simID_DeviceA].selected is True) or (tObjects[sim.links[self.link].simID_DeviceB].selected is True)):
                canvas.create_text(center[0], center[1], text=self.link, tags="networkObjects", fill=themes[themeVar.get()].label)


class selectorOb(object):                          #selection rectangle
    def __init__(self, selector):
        self.lineStart = selector[0]
        self.lineEnd = selector[1]

    def draw(self):
        offsetLineStart = [(self.lineStart[0] - offset[0])*zoom, (self.lineStart[1]-offset[1])*zoom]
        offsetLineEnd = [(self.lineEnd[0] - offset[0])*zoom, (self.lineEnd[1]-offset[1])*zoom]
        canvas.create_rectangle(offsetLineStart, offsetLineEnd, outline=themes[themeVar.get()].selector, width="2", tags="selector")


class Theme(object):                               #theme colors
    def __init__(self, canvas, nodeFill_normal, nodeOutline_normal, nodeFill_highlight, nodeOutline_highlight, linkColor_idle, linkColor_transfer, label, paneHandle, selector):
        self.canvas = canvas
        self.nodeFill_normal = nodeFill_normal
        self.nodeOutline_normal = nodeOutline_normal
        self.nodeFill_highlight = nodeFill_highlight
        self.nodeOutline_highlight = nodeOutline_highlight
        self.linkColor_idle = linkColor_idle
        self.linkColor_transfer = linkColor_transfer
        self.label = label
        self.paneHandle = paneHandle
        self.selector = selector


#-----------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------
#workspace event-handling functions
#-----------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------

def motion(event):                                 #event --- mouse motion
    global mousePos
    global mousePressed
    global tObjects
    global offset
    global selector
    mousePos = [(event.x+offset[0]*(zoom))*(1/zoom), (event.y+offset[1]*(zoom))*(1/zoom)]
    if mousePressed:
        if selector:
            selector = [selector[0],mousePos]
        drawEnv()


def click(event):                                  #event --- mouse press
    global mousePressed
    global mousePos
    global objectBeingDragged
    global selector
    canvas.focus_set()
    mousePressed = True
    foundDragSubject = False                       #prevents objects from getting eaten when clicked.  will only allow ONE central drag node to be selected during an overlapped click
    for nodeID in sim.nodeDict:
        if abs(mousePos[0] - tObjects[nodeID].centerPos[0]) < (tObjects[nodeID].width/2)*(1/zoom) and abs(mousePos[1] - tObjects[nodeID].centerPos[1]) < (tObjects[nodeID].width/2)*(1/zoom) and foundDragSubject is False:
            tObjects[nodeID].dragged = True
            foundDragSubject = True                #central drag node clicked.  dont map anything else DIRECTLY to mousePos.  only as an offset from mousePos
            objectBeingDragged = nodeID
    if objectBeingDragged:
        if not tObjects[objectBeingDragged].selected:
            for nodeID in sim.nodeDict:
                tObjects[nodeID].selected = False
                tObjects[objectBeingDragged].selected = True
    else:
        selector = [[(event.x+offset[0]*(zoom))*(1/zoom), (event.y+offset[1]*(zoom))*(1/zoom)], [(event.x+offset[0]*(zoom))*(1/zoom), (event.y+offset[1]*(zoom))*(1/zoom)]]

    # -------for printing object positions--------
    # positionsList=[]
    # for node in sim.nodeDict:
    #     positionsList.append("[" + sim.nodeDict[node].name + ", " + str(tObjects[node].centerPos) + "]")
    #     positionsList = sorted(positionsList)
    #     for position in positionsList:
    #         print(position)


def release(event):                                #event --- mouse release
    global mousePressed
    global objectBeingDragged
    global selector
    mousePressed = False
    for nodeID in sim.nodeDict:
        tObjects[nodeID].dragged = False
    objectBeingDragged = None
    if selector:
        for nodeID in sim.nodeDict:
            if (tObjects[nodeID].centerPos[0] > min(selector[1][0],selector[0][0])-25 and tObjects[nodeID].centerPos[0] < max(selector[1][0],selector[0][0])+25) and (tObjects[nodeID].centerPos[1] > min(selector[0][1],selector[1][1])-25 and tObjects[nodeID].centerPos[1] < max(selector[0][1],selector[1][1])+25):
                tObjects[nodeID].selected = True
            else :
                tObjects[nodeID].selected = False
    selector = None
    drawEnv()
    drawInfoBox()


def vMousewheel(event=0, sec = None):              #event --- mouse scroll vertical
    if sec is None:
        global offset
        if event.delta < 0:
            direction = 1.0
        else:
            direction = -1.0
        newOffset = offset[1] + direction*(abs(event.delta)**1.3)
        offset[1] =  max(0,min(newOffset,1200.0))


def hMousewheel(event=0, sec = None):              #event --- mouse scroll horizontal
    if sec is None:
        global offset
        if event.delta < 0:
            direction = 1.0
        else:
            direction = -1.0
        newOffset = offset[0] + direction*(abs(event.delta)**1.3)
        offset[0] =  max(0,min(newOffset,1200.0))


def step(event):                                   #event --- handles each simulation "step".  outputs log, calls step() on model
    if event.char == " ":
        sim.step()
        appendConsoleOutput()
    if event.char == "s":
        sendMessage()
        #appendConsoleOutput()
    drawEnv()
    drawInfoBox()


def interpolateVars():                             #called every "after(20, ...)".  used for background interpolations/iterations/draws
    global dashOffset
    dashOffset = (dashOffset + 1 ) % 16
    drawEnv()
    canvas.after(20, interpolateVars)
    #BE CAREFUL!!!! dont do too much here, or GUI might lock






#-----------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------
#core methods
#-----------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------

def drawEnv():
    defaultPos = 50
    for node in sim.nodeDict:                           #update node positions
        if not (node in tObjects):
            tnode = tNode(sim.nodeDict[node], [defaultPos, defaultPos])
            tObjects[node] = tnode
        else:
            if tObjects[node].dragged:
                originalPosition = tObjects[node].centerPos
                tObjects[node].centerPos = mousePos
                difference = [mousePos[0]-originalPosition[0], mousePos[1]-originalPosition[1]]
                for node_ in sim.nodeDict:
                    if tObjects[node_].selected and not(node_ == node) and tObjects[node].selected is True:
                        tObjects[node_].centerPos = [tObjects[node_].centerPos[0]+difference[0], tObjects[node_].centerPos[1]+difference[1]]

    for link in sim.links:                              #update link positions
        tlink = tLink(link, tObjects[sim.links[link].simID_DeviceA].centerPos, tObjects[sim.links[link].simID_DeviceB].centerPos)
        tObjects[link] = tlink

    canvas.delete("networkObjects")
    canvas.delete("selector")
    for link in sim.links:                              #draw links
         tObjects[link].draw()
    for node in sim.nodeDict:                           #draw nodes
        tObjects[node].draw()
    # print()
    if selector:                                        #draw selector box
        selectorOb(selector).draw()


def drawInfoBox():                                  #draw inspector

    #(poorly implimented---performance wise) recursive algorithm that returns a set of all networks "selected" for any group...
    #of selected nodes.
    #-------------------------------------------------------------------------------------------------------------------
    def find_nonConnector_ConnectedObjects(passed, visited=set(), result=set()):
        if not(passed in visited):
            if sim.nodeDict[passed].type == "CLIENT":
                visited.add(passed)
                for network in sim.networks:
                    if sim.Router(None, None, None).check_isDestinationInSubnet(sim.nodeDict[passed].IPAddress, sim.networks[network].networkID ,sim.networks[network].subnetMask):
                        if not(sim.isPrivateIPAddress(sim.networks[network].networkID)) or (findParentRouterNetwork(passed, set()) == network):
                            result.add(network)
            if sim.nodeDict[passed].type == "ROUTER":
                visited.add(passed)
                for network in sim.networks:
                    for IP in sim.nodeDict[passed].IP_Link:
                        if sim.Router(None, None, None).check_isDestinationInSubnet(IP, network ,sim.networks[network].subnetMask):
                            networksSelected.add(network)
                for PAT in sim.nodeDict[passed].link_NAT:                                   # << private networks too!
                    networksSelected.add(sim.nodeDict[passed].link_NAT[PAT].publicIP)
            if sim.nodeDict[passed].type == "HUB" or sim.nodeDict[passed].type == "SWITCH":
                visited.add(passed)
                for link in sim.nodeDict[passed].linkSet:
                    if not(sim.links[link].simID_DeviceA is nodeID):
                        if not(sim.nodeDict[sim.links[link].simID_DeviceA].type == "ROUTER"):
                            find_nonConnector_ConnectedObjects(sim.links[link].simID_DeviceA, visited, result)
                    else:
                        if not(sim.links[link].simID_DeviceA == "ROUTER"):
                            find_nonConnector_ConnectedObjects(sim.links[link].simID_DeviceB, visited, result)
        return result

    #recursive algorithm to find the parent router of any selected node
    #-------------------------------------------------------------------------------------------------------------------
    def findParentRouterNetwork(passed, visited=set(), reslink=None, result=[]):
        if not(passed in visited):
            if sim.nodeDict[passed].type == "ROUTER":
                result.append(sim.nodeDict[passed].link_IP[reslink][0])
            else:
                visited.add(passed)
                for link in sim.nodeDict[passed].linkSet:
                    if not(sim.links[link].simID_DeviceA is passed):
                        findParentRouterNetwork(sim.links[link].simID_DeviceA, visited, link, result)
                    else:
                        findParentRouterNetwork(sim.links[link].simID_DeviceB, visited, link, result)
        if len(result) is 0:
            return None
        else:
            return result[0]
    #-------------------------------------------------------------------------------------------------------------------

    text = ""
    if checkVar1.get() is 1:                                               #Get Selected Nodes InfoText
        for nodeID in sim.nodeDict:
            if tObjects[nodeID].selected is True:
                text = text+ sim.nodeDict[nodeID].getInfo() + "\n"

    if checkVar2.get() is 1:                                               #Get Selected Links InfoText
        linksSelected = set()
        for nodeID in sim.nodeDict:
            if tObjects[nodeID].selected:
                for linkID in sim.nodeDict[nodeID].linkSet:
                    linksSelected.add(linkID)
        if len(linksSelected) > 0:
            text = text + "\n" + "LINKS:" + "\n" + "-----------------------------------------------------------------" + "\n"
            for linkID in linksSelected:
                text = text + str(sim.links[linkID]) + "\n"

    if checkVar3.get() is 1:                                               #Get Selected Networks InfoText
        networksSelected = set()
        visited = set()
        for nodeID in sim.nodeDict:
            if tObjects[nodeID].selected:
                networksSelected = networksSelected.union(find_nonConnector_ConnectedObjects(nodeID, visited))
        if len(networksSelected) > 0:
            text = text + "\n" + "NETWORKS:" + "\n" + "-----------------------------------------------------------------"
        for network in networksSelected:
            if not(sim.isPrivateIPAddress(sim.networks[network].networkID)):
                text = text + str(sim.networks[network]) + "\n"
            else:
                text = text + "\n" + "private network: " + network + str(sim.networks[network])

    infoText.config(state=NORMAL)                                          #Update (write/draw) InfoText
    infoText.delete(1.0, END)
    infoText.insert(INSERT, text)
    infoText.config(state=DISABLED)


def appendConsoleOutput():                          #append console with updatedlog values
    flag = "apps"
    text = ""
    for out in sim.consoleOut[sim.stepCount[0]-1]:
        if out[1] == "apps" and checkVarApps.get() is 1 or out[1] == "steps" and checkVarSteps.get() is 1 or out[1] == "transport" and checkVarTransport.get() is 1 or out[1] == "default" and checkVarNetwork.get() is 1:
            text = text + out[0] + "\n"
            # if out[1] == "apps" or out[1] == "steps" or out[1] == "transport":
            #     text = text + out[0] + "\n"
    consoleText.config(state=NORMAL)
    consoleText.insert(END, text)
    consoleText.see(END)
    consoleText.config(state=DISABLED)


def drawConsoleOutput_fromZero():                   #redraw console when filters have changed
    flag = "apps"
    text = ""
    for consoleStep in sim.consoleOut:
        for out in consoleStep:
            if out[1] == "apps" and checkVarApps.get() is 1 or out[1] == "steps" and checkVarSteps.get() is 1 or out[1] == "transport" and checkVarTransport.get() is 1 or out[1] == "default" and checkVarNetwork.get() is 1:
                text = text + out[0] + "\n"
            # if out[1] == "apps" or out[1] == "steps" or out[1] == "transport":
            #     text = text + out[0] + "\n"
    consoleText.config(state=NORMAL)
    consoleText.delete(1.0, END)
    consoleText.insert(INSERT, text)
    consoleText.see(END)
    consoleText.config(state=DISABLED)


def setSample(sampleType):                          #load one of the pre-made sample networks from "sampleNetworks"

    if sampleType == "empty":
        nodes = sample.empty()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    if sampleType == "workspace":
        nodes = sample.workspace()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    if sampleType == "starHub":
        nodes = sample.starHub()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    if sampleType == "starSwitch":
        nodes = sample.starSwitch()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    if sampleType == "bus":
        nodes = sample.bus()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    if sampleType == "tree":
        nodes = sample.tree()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    if sampleType == "multiNet1":
        nodes = sample.multiNetwork()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    if sampleType == "pat":
        nodes = sample.pat()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    if sampleType == "multiNet2":
        nodes = sample.multiNetworkBig()
        for node in nodes:
            tObjects[node[0]] = tNode(sim.nodeDict[node[0]], node[1])
    refreshFromList()
    drawConsoleOutput_fromZero()


def reset():                                        # ----NOT USED------
    global tObjects
    global mousePos
    global mousePressed
    global objectBeingDragged
    global offset
    global selector
    global dashOffset
    global consoleText

    tObjects = {}
    mousePos = [0,0]
    mousePressed = False
    objectBeingDragged = None
    offset = [0.0,0.0]
    selector = None
    dashOffset = 0

    sim.consoleOut = []
    consoleText.config(state=NORMAL)
    consoleText.delete(1.0, END)
    consoleText.see(END)
    consoleText.config(state=DISABLED)


#-----------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------
#GUI builders, helpers, classes
#-----------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------

def defineThemes():
    themes["dark"] = Theme("gray10", 'RoyalBlue4', 'RoyalBlue3', 'RoyalBlue4', 'white', 'RoyalBlue4', 'gray90', 'white', "gray30", "gray80")
    themes["light"] = Theme("white", 'LightSkyBlue3', 'LightSkyBlue4', 'LightSkyBlue4', 'RoyalBlue4', 'LightSkyBlue1', 'RoyalBlue4', 'black', 'grey85', "gray30")


#this makes the Node Creation form GUI components and events/callbacks.  yahhhh.....this SHOULD all be in a class.
def makeCreationForm():
    values = ["pad","/30","/29","/28","/27","/26","/25","/24","/23","/22","/21","/20","/19","/18","/17","/16"]
    selectedSubnetSizeVar = StringVar(createNodeCanvas)
    selectedSubnetSizeVar.set(values[0])
    availibility = ttk.Label(createNodeCanvas, text="--select subnet size--", foreground="gray")
    def reqDialog():
        d = MyDialog(canvas)
        result = d.result
        if result:
            clientIPAddressInput.delete(0, END)
            clientSubnetMaskInput.delete(0, END)
            clientDefaultGatewayInput.delete(0,END)
            clientIPAddressInput.insert(0, d.result[0])
            clientSubnetMaskInput.insert(0,d.result[1])
            clientDefaultGatewayInput.insert(0,d.result[2])

    def networkCall():
        sim.createNetwork(networkPrefixInput.get(), 30-int(selectedSubnetSizeVar.get()[1:]))
        print("created network")

    def selectedSubnetSize(*args):
        availibility.config(text="number of hosts: " + str((2**(30-int(selectedSubnetSizeVar.get()[1:])+2))-2), foreground="black")
        print("selected subnet size")

    def linkCall():
        device1 = linkDevice1Input.get()
        device2 = linkDevice2Input.get()
        if not(sim.nodeDict[device1] and sim.nodeDict[device2]):
            print "error, one of these devices doesnt exist"
            return False
        if device1 is device2:
            print "error, cannot link to self"
            return False
        if sim.nodeDict[device1].type == "CLIENT" and sim.nodeDict[device2].type == "CLIENT":
            print "error, cannot directly link client to client in this sim (currently)"
            return False
        if (sim.nodeDict[device1].type == "CLIENT" and sim.nodeDict[device1].linkSet) or (sim.nodeDict[device2].type == "CLIENT" and sim.nodeDict[device2].linkSet):
                print "error, client has already been assigned a link.  cant have multiple links (currently)"
                return False
        for link in sim.links:
            print(link + "   " + sim.links[link].simID_DeviceA)
            if (sim.links[link].simID_DeviceA == device1 or sim.links[link].simID_DeviceA == device2) and (sim.links[link].simID_DeviceB == device1 or sim.links[link].simID_DeviceB == device2):
                print "error, link between these devices already exists"
                return False
        sim.createLink(device1, device2, linkType="basic-basic")

    def hubCall():
        name = hubNameInput.get()
        sim.createHub(name)

    def switchCall():
        name = switchNameInput.get()
        sim.createSwitch(name)

    def clientCall():
        name = clientNameInput.get()
        macAddress = clientMacAddressInput.get()
        IPAddress = clientIPAddressInput.get()
        subnetMask = clientSubnetMaskInput.get()
        defaultGateway = clientDefaultGatewayInput.get()
        sim.createClient(macAddress, IPAddress, subnetMask, defaultGateway, name)

    def routerCall():
        name = routerNameInput.get()
        macAddress = routerMacAddressInput.get()
        defaultGateway = routerDefaultGatewayInput.get()
        sim.createRouter(macAddress, IPAddress, subnetMask, defaultGateway, name)
        sim.createRouter(macAddress, defaultGateway, name)

    def makeentry(parent, caption, row=0, column=0, width=None, **options):
        ttk.Label(parent, text=caption).grid(row=row, column=column, sticky=W)
        entry = ttk.Entry(parent, **options)
        if width:
            entry.config(width=width)
        entry.grid(row=row, column=column+1)
        return entry

    def displayHosts(parent, caption, row=0, column=0, width=None, **options):
        entry = ttk.Entry(parent, **options)
        if width:
            entry.config(width=width)
        entry.grid(row=row, column=column+1)
        return entry

    currentRow = 0
    hubNameInput = makeentry(createNodeCanvas, "name:", row=currentRow+0, width=20)
    createHubButton = ttk.Button(createNodeCanvas, text="Hub >>", width=8, command=hubCall)
    createHubButton.grid(row=currentRow+1, column=1, sticky=E)
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+2, column=0,sticky="ew")
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+2, column=1,sticky="ew")

    currentRow += 4
    switchNameInput = makeentry(createNodeCanvas, "name:", row=currentRow+0, width=20)
    createSwitchButton = ttk.Button(createNodeCanvas, text="Switch >>", width=8, command=switchCall)
    createSwitchButton.grid(row=currentRow+1, column=1, sticky=E)
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+2, column=0,sticky="ew")
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+2, column=1,sticky="ew")

    currentRow += 4
    clientNameInput = makeentry(createNodeCanvas, "Name:", row=currentRow+0, width=20)
    clientMacAddressInput = makeentry(createNodeCanvas, "MAC Address:", row=currentRow+1, width=20)
    clientIPAddressInput = makeentry(createNodeCanvas, "IP Address:", row=currentRow+2, width=20)
    clientSubnetMaskInput = makeentry(createNodeCanvas, "Subnet Mask:", row=currentRow+3, width=20)
    clientDefaultGatewayInput = makeentry(createNodeCanvas, "Default Gateway:", row=currentRow+4, width=20)
    createClientButton = ttk.Button(createNodeCanvas, text="Client >>", width=8, command=clientCall)
    createClientButton.grid(row=currentRow+5, column=1, sticky=E)
    getButton = ttk.Button(createNodeCanvas, text="~", width=1, command=reqDialog)
    getButton.grid(row=currentRow+2, column=2, sticky=W)
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+6, column=0,sticky="ew")
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+6, column=1,sticky="ew")

    currentRow += 8
    routerNameInput = makeentry(createNodeCanvas, "name:", row=currentRow+0, width=20)
    routerMacAddressInput = makeentry(createNodeCanvas, "MAC Address:", row=currentRow+1, width=20)
    routerDefaultGatewayInput = makeentry(createNodeCanvas, "Default Gateway:", row=currentRow+2, width=20)
    createRouterButton = ttk.Button(createNodeCanvas, text="Router >>", width=8, command=routerCall)
    createRouterButton.grid(row=currentRow+3, column=1, sticky=E)
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+4, column=0,sticky="ew")
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+4, column=1,sticky="ew")

    currentRow += 5
    linkDevice1Input = makeentry(createNodeCanvas, "Device 1:", row=currentRow+0, width=20)
    linkDevice2Input = makeentry(createNodeCanvas, "Device 2:", row=currentRow+1, width=20)
    createLinkButton = ttk.Button(createNodeCanvas, text="Link >>", width=8, command=linkCall)
    createLinkButton.grid(row=currentRow+2, column=1, sticky=E)
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+3, column=0,sticky="ew")
    ttk.Separator(createNodeCanvas, orient=HORIZONTAL).grid(row=currentRow+3, column=1,sticky="ew")

    currentRow += 4
    networkPrefixInput = makeentry(createNodeCanvas, "Network Prefix:", row=currentRow+0, width=20)
    ttk.Label(createNodeCanvas, text="Subnet Size").grid(row=currentRow+1, column=0)
    subnetSizeOptions = ttk.OptionMenu(createNodeCanvas, selectedSubnetSizeVar, *values)
    selectedSubnetSizeVar.set(values[1])
    selectedSubnetSizeVar.trace("w", selectedSubnetSize)
    subnetSizeOptions.grid(row=currentRow+1, column=1, sticky=EW)
    ttk.Label(createNodeCanvas, text="Host Number:").grid(row=currentRow+2, column=0)
    availibility.grid(row=currentRow+2, column=1)
    createNetworkButton = ttk.Button(createNodeCanvas, text="Allocate Subnet", width=15, command=networkCall)
    createNetworkButton.grid(row=currentRow+3, column=1, sticky=E)


class MyDialog(tkSimpleDialog.Dialog):          #popup dialog box for selecting host IP's.

    def updateValues(self, *args):
        print "updated to " + self.selectedHop.get()
        self.selectionIPs = sim.networks[self.selectedHop.get()].hostIPs
        for x in range (0,len(self.selectionIPs)):
            if self.selectionIPs[x] in sim.IPSet:
                self.selectionIPs[x] = self.selectionIPs[x] + " --set--"
        self.w2.config(state='readonly')
        self.w2.set(self.selectionIPs[0])
        self.w2.config(values=self.selectionIPs)
        self.subMask.config(text=sim.networks[self.selectedHop.get()].subnetMask, foreground="black")
        self.defaultGateway.config(text=sim.networks[self.selectedHop.get()].hostIPs[0], foreground="black")
        self.selectionMade = True


    def body(self, master):
        self.selectionMade = False
        Label(master, text="Network:").grid(row=0, sticky=W)
        Label(master, text="Subnet Mask:").grid(row=1, sticky=W)
        Label(master, text="Default Gateway:").grid(row=2, sticky=W)
        Label(master, text="IP Address:").grid(row=3, sticky=W)

        self.values = ["none"]
        for network in sim.networks:
            self.values.append(network)
        self.selectedHop = StringVar(master)
        #self.selectedHop.set(self.values[0])
        self.selectedHop.trace("w", self.updateValues)
        self.hopOptions = OptionMenu(master, self.selectedHop, *self.values)
        self.hopOptions.grid(row=0, column=1, sticky=EW)

        self.values2 = ["---.---.---.---"]
        self.w2 = ttk.Combobox(master, background="white", values=self.values2)
        self.w2.grid(row=3, column=1, sticky=EW)
        self.w2.config(state=DISABLED)

        self.values3 = []
        self.subMask = Label(master, text="---.---.---.---", foreground="gray")
        self.subMask.grid(row=1, column=1, sticky=EW)

        values4 = []
        self.defaultGateway = Label(master, text="---.---.---.---", foreground="gray")
        self.defaultGateway.grid(row=2, column=1, sticky=EW)

    def apply(self):
        if self.selectionMade:
            self.result = [self.w2.get(), self.subMask["text"], self.defaultGateway["text"]]
            for x in range(0,len(self.result)):
                if self.result[x].endswith(" --set--", 0, len(self.result[x])):
                    self.result[x] = self.result[x][:-8]





#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
#DECLARING GUI ---SETUP---
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------

print("begin")
defineThemes()
animation = Tk()
paneWin = PanedWindow( showhandle=False, background="gray30", sashwidth=2, sashpad=2, borderwidth=0, width=1200, height=700)
paneWin.pack(fill=BOTH, expand=1)



#-------------------------------------------------------------
#workspace canvas
#-------------------------------------------------------------
frame=Frame(animation,width=1,height=1)
canvas = Canvas(animation, highlightthickness=0, width=300, height=300, background="gray10")
canvas.config(width=600,height=400, yscrollcommand=vMousewheel, xscrollcommand=hMousewheel)



tabView = ttk.Notebook(animation)
#----------------------------------------------------------------
#inspector  -notebook page
#----------------------------------------------------------------
page1 = ttk.Frame(tabView)
infoText = Text(page1, wrap="none", height=300)
infoText.grid(row=0, columnspan=4, sticky=NSEW)
infoCanvasControls = Canvas(page1, highlightthickness=0)
infoCanvasControls.grid(row=1, columnspan=4, sticky=EW)

checkCaption = Label(infoCanvasControls, text = "filter:")
checkCaption.grid(row=1, column=0)

checkVar1 = IntVar()
checkButton1 = Checkbutton(infoCanvasControls, text="Nodes", variable=checkVar1, justify = LEFT, command=drawInfoBox)
checkButton1.grid(row=1, column=1)

checkVar2 = IntVar()
checkButton2 = Checkbutton(infoCanvasControls, text="Links", variable=checkVar2, justify = LEFT, command=drawInfoBox)
checkButton2.grid(row=1, column=2)

checkVar3 = IntVar()
checkButton3 = Checkbutton(infoCanvasControls, text="Networks", variable=checkVar3, justify = LEFT, command=drawInfoBox)
checkButton3.grid(row=1, column=3)

Grid.rowconfigure(page1, 0, weight=1)
Grid.columnconfigure(page1, 0, weight=1)
Grid.columnconfigure(infoCanvasControls, 0, weight=1)
Grid.columnconfigure(infoCanvasControls, 1, weight=1)
Grid.columnconfigure(infoCanvasControls, 2, weight=1)
Grid.columnconfigure(infoCanvasControls, 3, weight=1)

#----------------------------------------------------------------
#console    -notebook page
#----------------------------------------------------------------
page2 = ttk.Frame(tabView)
consoleText = Text(page2, width = 500, height=500, wrap="none", state="disabled")
consoleText.grid(row=0, columnspan=4, sticky=NSEW)
consoleCanvasControls = Canvas(page2, highlightthickness=0)
consoleCanvasControls.grid(row=1, columnspan=4, sticky=EW)

checkCaption = Label(consoleCanvasControls, text = "filter:")
checkCaption.grid(row=1, column=0)

checkVarSteps = IntVar()
checkButtonSteps = Checkbutton(consoleCanvasControls, text="steps", variable=checkVarSteps, justify = LEFT, command=drawConsoleOutput_fromZero)
checkButtonSteps.grid(row=1, column=1)

checkVarApps = IntVar()
checkButtonApps = Checkbutton(consoleCanvasControls, text="apps", variable=checkVarApps, justify = LEFT, command=drawConsoleOutput_fromZero)
checkButtonApps.grid(row=1, column=2)

checkVarTransport = IntVar()
checkButtonTransport = Checkbutton(consoleCanvasControls, text="transport", variable=checkVarTransport, justify = LEFT, command=drawConsoleOutput_fromZero)
checkButtonTransport.grid(row=1, column=3)

checkVarNetwork = IntVar()
checkButtonNetwork = Checkbutton(consoleCanvasControls, text="internet/network", variable=checkVarNetwork, justify = LEFT, command=drawConsoleOutput_fromZero)
checkButtonNetwork.grid(row=1, column=4)

Grid.rowconfigure(page2, 0, weight=1)
Grid.columnconfigure(page2, 0, weight=1)
Grid.columnconfigure(consoleCanvasControls, 0, weight=1)
Grid.columnconfigure(consoleCanvasControls, 1, weight=1)
Grid.columnconfigure(consoleCanvasControls, 2, weight=1)
Grid.columnconfigure(consoleCanvasControls, 3, weight=1)
Grid.columnconfigure(consoleCanvasControls, 4, weight=1)



#----------------------------------------------------------------
#creation    -notebook page
#----------------------------------------------------------------
page3 = ttk.Frame(tabView)
createNodeCanvas = ttk.Frame(page3, borderwidth=2, width=300, height=300)
createNodeCanvas.pack( side=TOP )
makeCreationForm()

#----------------------------------------------------------------
#settings    -notebook page
#----------------------------------------------------------------
page4 = Frame(tabView, background="gray90")
settingsCanvas = Frame(page4, borderwidth=2, width=300, height=300, background="gray90")
settingsCanvas.pack( side=TOP )

def updateScale(val):
    global zoom
    global nodeWidth
    zoom = float(val)
    for node in sim.nodeDict:
        tObjects[node].width = nodeWidth*zoom

def changeTheme(*args):
    canvas.config(background=themes[themeVar.get()].canvas)
    paneWin.config(background=themes[themeVar.get()].paneHandle)


Label(settingsCanvas, text="Zoom:", background="gray90").grid(row=0, column=0, sticky=W)
slider = Scale(settingsCanvas, orient='horizontal', from_=0.2, to=1, resolution=0.01, background="gray90", length=150, command=updateScale)
slider.set(zoom)
slider.grid(row=0, column=1, columnspan=2, sticky=W)

Label(settingsCanvas, text="show labels (nodes):", background="gray90").grid(row=1, column=0, sticky=W)
checkVarShowLabelsName = IntVar()
checkVarShowLabelsName.set(1)
checkButtonShowLabelsName = Checkbutton(settingsCanvas, text="", variable=checkVarShowLabelsName, justify = LEFT, command=drawEnv, background="gray90")
checkButtonShowLabelsName.grid(row=1, column=1,columnspan=2, sticky=W)

Label(settingsCanvas, text="show labels (links):", background="gray90").grid(row=2, column=0, sticky=W)
checkVarShowLabelsLinks = IntVar()
checkButtonShowLabelsLinks = Checkbutton(settingsCanvas, text="", variable=checkVarShowLabelsLinks, justify = LEFT, command=drawEnv, background="gray90")
checkButtonShowLabelsLinks.grid(row=2, column=1,columnspan=2, sticky=W)

Label(settingsCanvas, text="filter labels (selection-only):", background="gray90").grid(row=3, column=0, sticky=W)
checkVarFilterLabels = IntVar()
checkButtonFilterLabels = Checkbutton(settingsCanvas, text="", variable=checkVarFilterLabels, justify = LEFT, command=drawEnv, background="gray90")
checkButtonFilterLabels.grid(row=3, column=1,columnspan=2, sticky=W)

Label(settingsCanvas, text="theme:", background="gray90").grid(row=4, column=0, sticky=W)
themeVar = StringVar()
themeVar.trace("w", changeTheme)
themeVar.set("light")
radLight = Radiobutton(settingsCanvas, text="light", background="gray90", variable=themeVar, value="light")
radDark = Radiobutton(settingsCanvas, text="dark", background="gray90", variable=themeVar, value="dark")
radLight.grid(row=4, column=1, sticky=W)
radDark.grid(row=4, column=2, sticky=W)

Label(settingsCanvas, text="arrowhead visible (links):", background="gray90").grid(row=5, column=0, sticky=W)
checkVarArrowhead = IntVar()
checkButtonArrowhead = Checkbutton(settingsCanvas, text="", variable=checkVarArrowhead, justify = LEFT, background="gray90")
checkButtonArrowhead.grid(row=5, column=1,columnspan=2, sticky=W)

#----------------------------------------------------------------
#sending messages    -notebook page
#----------------------------------------------------------------
page5 = Frame(tabView, background="gray90")
commandCanvas = Frame(page5, borderwidth=2, width=300, height=300, background="gray90")
commandCanvas.pack( side=TOP )
Label(commandCanvas, text="send message:", background="gray90").grid(row=0, column=1, sticky=W)

fromValues = ["none"]
messageTypesList = ["default", "get_resp", "TCP"]                                                 #TODO:  all of this should probably be in a class.......
sortedNaturalNames = ["none"]
#natural sorting re --- borrowed from...
#http://stackoverflow.com/questions/2545532/python-analog-of-natsort-function-sort-a-list-using-a-natural-order-algorithm
def natural_key(string_):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_[0])]

def refreshFromList():
    fromValues = []
    for node in sim.nodeDict:
        if sim.nodeDict[node].type == "CLIENT":
            fromValues.append([sim.nodeDict[node].name, node])
    if len(fromValues) is 0:
        fromValues = [["none", None]]
    global sortedNaturalNames
    sortedNaturalNames = sorted(fromValues, key=natural_key)
    fromNodeOptionsMenu.config(values=[i[0] for i in sortedNaturalNames])
    toNodeOptionsMenu.config(values=[i[0] for i in sortedNaturalNames])

def sendMessage():
    fromSimID = sortedNaturalNames[fromNodeOptionsMenu.current()][1]
    toSimID = sortedNaturalNames[toNodeOptionsMenu.current()][1]
    messageType = messageTypesList[messageTypeOptionsMenu.current()]
    toPort = toPortEntry.get()
    fromPort = fromPortEntry.get()
    if fromSimID is None or toSimID is None:
        print("Error:  no valid source/destination node selected")
    else:
        sim.initiateMessageTest(fromSimID, toSimID, fromPort, toPort, messageType)


Label(commandCanvas, text="from:", background="gray90").grid(row=1, column=0, sticky=E)
fromNodeOptionsMenu = ttk.Combobox(commandCanvas, background="white", values=fromValues, postcommand=refreshFromList)
fromNodeOptionsMenu.grid(row=1, column=1, sticky=EW)
fromNodeOptionsMenu.config(state="readonly")

Label(commandCanvas, text="port:", background="gray90").grid(row=1, column=2, sticky=E)
fromPortEntry = Entry(commandCanvas, width=10)
fromPortEntry.grid(row=1, column=3, sticky=EW)
fromPortEntry.insert(0, "0")

Label(commandCanvas, text="to:", background="gray90").grid(row=2, column=0, sticky=E)
toNodeOptionsMenu = ttk.Combobox(commandCanvas, background="white", values=fromValues, postcommand=refreshFromList)
toNodeOptionsMenu.grid(row=2, column=1, sticky=EW)
toNodeOptionsMenu.config(state="readonly")

Label(commandCanvas, text="port:", background="gray90").grid(row=2, column=2, sticky=E)
toPortEntry = Entry(commandCanvas, width=10)
toPortEntry.grid(row=2, column=3, sticky=EW)
toPortEntry.insert(0, "0")

fromNodeOptionsMenu.set(fromValues[0])
toNodeOptionsMenu.set(fromValues[0])
refreshFromList()

Label(commandCanvas, text="type", background="gray90").grid(row=3, column=0, sticky=E)
messageTypeOptionsMenu = ttk.Combobox(commandCanvas, background="white", values=messageTypesList)
messageTypeOptionsMenu.grid(row=3, column=1, sticky=EW)
messageTypeOptionsMenu.config(state="readonly")
messageTypeOptionsMenu.set(messageTypesList[0])

messageButton = Button(commandCanvas, text="send message", width=14, command=sendMessage)
messageButton.grid(row=4, column=1, sticky=EW)

#-------------------------------------------------------------
#pack notebook
#-------------------------------------------------------------
tabView.add(page1, text='Inspector')
tabView.add(page2, text='Log')
tabView.add(page3, text='Create')
tabView.add(page4, text='Settings')
tabView.add(page5, text='Commands')


#-------------------------------------------------------------
#Pack Paned Window
#-------------------------------------------------------------
paneWin.add(tabView)
paneWin.add(canvas)
paneWin.sash_place(0, 390, 200)


#-------------------------------------------------------------
# create menu-bar items
#-------------------------------------------------------------
menubar = Menu(animation)
filemenu = Menu(menubar, tearoff=0)
simmenu = Menu(menubar, tearoff=0)
viewmenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="Open", command=step)
filemenu.add_command(label="Close", command=step)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=animation.quit)
menubar.add_cascade(label="File", menu=filemenu)

simmenu.add_command(label="Empty", command=lambda: setSample("empty"))
simmenu.add_command(label="workspace", command=lambda: setSample("workspace"))
simmenu.add_command(label="Star (hub)", command=lambda: setSample("starHub"))
simmenu.add_command(label="Star (switch)", command=lambda: setSample("starSwitch"))
simmenu.add_command(label="Bus", command=lambda: setSample("bus"))
simmenu.add_command(label="Tree", command=lambda: setSample("tree"))
simmenu.add_command(label="Multi-Network", command=lambda: setSample("multiNet1"))
simmenu.add_command(label="PAT", command=lambda: setSample("pat"))
simmenu.add_command(label="Multi-Network (BIG)", command=lambda: setSample("multiNet2"))
simmenu.add_separator()
menubar.add_cascade(label="Sims", menu=simmenu)

animation.config(menu=menubar)






#-------------------------------------------------------------
# bindings
#-------------------------------------------------------------
canvas.focus_set()
canvas.bind('<Motion>', motion)
canvas.bind("<Button-1>", click)
canvas.bind("<ButtonRelease-1>", release)
canvas.bind("<Key>", step)
canvas.bind("<MouseWheel>", vMousewheel)
canvas.bind('<Shift-MouseWheel>', hMousewheel)
infoText.bind("<1>", lambda event: infoText.focus_set())



#-------------------------------------------------------------
# init
#-------------------------------------------------------------
interpolateVars()
drawEnv()
animation.mainloop()
print("closing.")