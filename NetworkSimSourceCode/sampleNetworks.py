import networkSim as sim




def empty():
    sim.reset()

    #Positions
    return[]







def workspace():
    # sim.reset()
    # network1 = sim.createNetwork("10.0", 3)
    # switch1 = sim.createSwitch("switch1")
    # client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")
    # client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    # client3 = sim.createClient("MACADDRESS3", network1.hostIPs[3], network1.subnetMask, network1.hostIPs[0], "client3")
    # client4 = sim.createClient("MACADDRESS4", network1.hostIPs[4], network1.subnetMask, network1.hostIPs[0], "client4")


    # linka = sim.createLink(client1, switch1, linkType="basic-basic")
    # linkb = sim.createLink(client2, switch1, linkType="basic-basic")
    # linkc = sim.createLink(client3, switch1, linkType="basic-basic")
    # linkd = sim.createLink(client4, switch1, linkType="basic-basic")


    # #Positions
    # return[     [client1, [52.0, 166.0]],
    #             [client2, [193.0, 100.0]],
    #             [client3, [224.0, 164.0]],
    #             [client4, [196.0, 229.0]],
    #             [switch1, [121.0, 166.0]]
    #       ]



    sim.reset()
    network1 = sim.createNetwork("100.100", 3)
    client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")
    client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    client3 = sim.createClient("MACADDRESS3", network1.hostIPs[3], network1.subnetMask, network1.hostIPs[0], "client3")
    client4 = sim.createClient("MACADDRESS4", network1.hostIPs[4], network1.subnetMask, network1.hostIPs[0], "client4")
    client5 = sim.createClient("MACADDRESS5", network1.hostIPs[5], network1.subnetMask, network1.hostIPs[0], "client5")
    client6 = sim.createClient("MACADDRESS6", network1.hostIPs[6], network1.subnetMask, network1.hostIPs[0], "client6")
    client7 = sim.createClient("MACADDRESS7", network1.hostIPs[7], network1.subnetMask, network1.hostIPs[0], "client7")
    client8 = sim.createClient("MACADDRESS8", network1.hostIPs[8], network1.subnetMask, network1.hostIPs[0], "client8")
    client9 = sim.createClient("MACADDRESS9", network1.hostIPs[9], network1.subnetMask, network1.hostIPs[0], "client9")
    client10 = sim.createClient("MACADDRESS10", network1.hostIPs[10], network1.subnetMask, network1.hostIPs[0], "client10")

    linka = sim.createLink(client1, client2, linkType="basic-basic")
    linkb = sim.createLink(client2, client3, linkType="basic-basic")
    linkc = sim.createLink(client3, client4, linkType="basic-basic")
    linkd = sim.createLink(client4, client5, linkType="basic-basic")
    linkf = sim.createLink(client5, client6, linkType="basic-basic")
    linkg = sim.createLink(client6, client7, linkType="basic-basic")
    linkh = sim.createLink(client7, client8, linkType="basic-basic")
    linki = sim.createLink(client8, client9, linkType="basic-basic")
    linkj = sim.createLink(client9, client10, linkType="basic-basic")
    linkk = sim.createLink(client10, client1, linkType="basic-basic")

    #Positions
    return[ [    client2,    [460,100]     ],
            [    client1,    [300,100]     ],
            [    client3,    [560,400]     ],
            [    client4,    [450,470]     ],
            [    client5,    [210,400]     ],
            [    client6,    [560,190]     ],
            [    client7,    [210,190]     ],
            [    client8,    [180,300]     ],
            [    client9,    [310,470]     ],
            [    client10,   [590,300]     ]
          ]


def tree():
    sim.reset()
    network1 = sim.createNetwork("100.100", 4)
    hub1 = sim.createHub("hub1")
    hub2 = sim.createHub("hub2")
    hub3 = sim.createHub("hub3")
    hub4 = sim.createHub("hub4")
    hub5 = sim.createHub("hub5")
    client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")
    client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    client3 = sim.createClient("MACADDRESS3", network1.hostIPs[3], network1.subnetMask, network1.hostIPs[0], "client3")
    client4 = sim.createClient("MACADDRESS4", network1.hostIPs[4], network1.subnetMask, network1.hostIPs[0], "client4")
    client5 = sim.createClient("MACADDRESS5", network1.hostIPs[5], network1.subnetMask, network1.hostIPs[0], "client5")
    client6 = sim.createClient("MACADDRESS6", network1.hostIPs[6], network1.subnetMask, network1.hostIPs[0], "client6")
    client7 = sim.createClient("MACADDRESS7", network1.hostIPs[7], network1.subnetMask, network1.hostIPs[0], "client7")
    client8 = sim.createClient("MACADDRESS8", network1.hostIPs[8], network1.subnetMask, network1.hostIPs[0], "client8")
    client9 = sim.createClient("MACADDRESS9", network1.hostIPs[9], network1.subnetMask, network1.hostIPs[0], "client9")
    client10 = sim.createClient("MACADDRESS10", network1.hostIPs[10], network1.subnetMask, network1.hostIPs[0], "client10")
    client11 = sim.createClient("MACADDRESS11", network1.hostIPs[11], network1.subnetMask, network1.hostIPs[0], "client11")
    client12 = sim.createClient("MACADDRESS12", network1.hostIPs[12], network1.subnetMask, network1.hostIPs[0], "client12")
    client13 = sim.createClient("MACADDRESS13", network1.hostIPs[13], network1.subnetMask, network1.hostIPs[0], "client13")
    client14 = sim.createClient("MACADDRESS14", network1.hostIPs[14], network1.subnetMask, network1.hostIPs[0], "client14")
    client15 = sim.createClient("MACADDRESS15", network1.hostIPs[15], network1.subnetMask, network1.hostIPs[0], "client15")

    linka = sim.createLink(hub1, hub2, linkType="basic-basic")
    linkb = sim.createLink(hub1, hub3, linkType="basic-basic")
    linkc = sim.createLink(hub3, hub4, linkType="basic-basic")
    linkc = sim.createLink(hub3, hub5, linkType="basic-basic")
    linkd = sim.createLink(client1, hub2, linkType="basic-basic")
    linkf = sim.createLink(client2, hub2, linkType="basic-basic")
    linkg = sim.createLink(client3, hub2, linkType="basic-basic")
    linkh = sim.createLink(client4, hub3, linkType="basic-basic")
    linki = sim.createLink(client5, hub3, linkType="basic-basic")
    linkj = sim.createLink(client6, hub3, linkType="basic-basic")
    linkk = sim.createLink(client7, hub4, linkType="basic-basic")
    linkl = sim.createLink(client8, hub4, linkType="basic-basic")
    linkm = sim.createLink(client9, hub4, linkType="basic-basic")
    linkn = sim.createLink(client10, hub5, linkType="basic-basic")
    linko = sim.createLink(client11, hub5, linkType="basic-basic")
    linkp = sim.createLink(client12, hub5, linkType="basic-basic")
    linkq = sim.createLink(client13, hub5, linkType="basic-basic")
    linkr = sim.createLink(client14, hub5, linkType="basic-basic")
    links = sim.createLink(client15, hub5, linkType="basic-basic")

    #Positions
    return[ [hub3,          [499.15851632505826, 246.0]     ],
            [client2,       [734.1585163250583, 233.0]      ],
            [client11,      [900.6759046806613, 312.0]      ],
            [client5,       [549.1788391427652, 302]        ],
            [hub5,          [860.6759046806613, 371.0]      ],
            [client9,       [212.35180936132275, 424.0]     ],
            [client1,       [813.1585163250583, 160.0]      ],
            [hub1,          [502.1363907997415, 105.0]      ],
            [client6,       [499.17883914276524, 323]       ],
            [client12,      [936.6759046806613, 357.0]      ],
            [client8,       [143.35180936132275, 408.0]     ],
            [client15,      [814.6759046806613, 434.0]      ],
            [client4,       [446.17883914276524, 304]       ],
            [client14,      [925.6759046806613, 413.0]      ],
            [hub2,          [747.1585163250583, 174.0]      ],
            [client13,      [794.6759046806613, 385.0]      ],
            [hub4,          [185.35180936132275, 358.0]     ],
            [client10,      [876.6759046806613, 444.0]      ],
            [client7,       [115.35180936132275, 352.0]     ],
            [client3,       [802.1585163250583, 216.0]      ],
            [hub3,          [499.15851632505826, 246.0]     ]
          ]






def starHub():
    sim.reset()
    network1 = sim.createNetwork("100.100", 3)
    hub1 = sim.createHub("hub1")
    client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")
    client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    client3 = sim.createClient("MACADDRESS3", network1.hostIPs[3], network1.subnetMask, network1.hostIPs[0], "client3")
    client4 = sim.createClient("MACADDRESS4", network1.hostIPs[4], network1.subnetMask, network1.hostIPs[0], "client4")
    client5 = sim.createClient("MACADDRESS5", network1.hostIPs[5], network1.subnetMask, network1.hostIPs[0], "client5")
    client6 = sim.createClient("MACADDRESS6", network1.hostIPs[6], network1.subnetMask, network1.hostIPs[0], "client6")
    client7 = sim.createClient("MACADDRESS7", network1.hostIPs[7], network1.subnetMask, network1.hostIPs[0], "client7")
    client8 = sim.createClient("MACADDRESS8", network1.hostIPs[8], network1.subnetMask, network1.hostIPs[0], "client8")
    client9 = sim.createClient("MACADDRESS9", network1.hostIPs[9], network1.subnetMask, network1.hostIPs[0], "client9")
    client10 = sim.createClient("MACADDRESS10", network1.hostIPs[10], network1.subnetMask, network1.hostIPs[0], "client10")

    linka = sim.createLink(client1, hub1, linkType="basic-basic")
    linkb = sim.createLink(client2, hub1, linkType="basic-basic")
    linkc = sim.createLink(client3, hub1, linkType="basic-basic")
    linkd = sim.createLink(client4, hub1, linkType="basic-basic")
    linkf = sim.createLink(client5, hub1, linkType="basic-basic")
    linkg = sim.createLink(client6, hub1, linkType="basic-basic")
    linkh = sim.createLink(client7, hub1, linkType="basic-basic")
    linki = sim.createLink(client8, hub1, linkType="basic-basic")
    linkj = sim.createLink(client9, hub1, linkType="basic-basic")
    linkk = sim.createLink(client10, hub1, linkType="basic-basic")

    #Positions
    return[ [    client2,    [460,100]     ],
            [    client1,    [300,100]     ],
            [    hub1,    [390,300]     ],
            [    client3,    [560,400]     ],
            [    client4,    [450,470]     ],
            [    client5,    [210,400]     ],
            [    client6,    [560,190]     ],
            [    client7,    [210,190]     ],
            [    client8,    [180,300]     ],
            [    client9,    [310,470]     ],
            [    client10,   [590,300]     ]
          ]







def starSwitch():
    sim.reset()
    network1 = sim.createNetwork("100.100", 3)
    switch1 = sim.createSwitch("switch1")
    client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")
    client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    client3 = sim.createClient("MACADDRESS3", network1.hostIPs[3], network1.subnetMask, network1.hostIPs[0], "client3")
    client4 = sim.createClient("MACADDRESS4", network1.hostIPs[4], network1.subnetMask, network1.hostIPs[0], "client4")
    client5 = sim.createClient("MACADDRESS5", network1.hostIPs[5], network1.subnetMask, network1.hostIPs[0], "client5")
    client6 = sim.createClient("MACADDRESS6", network1.hostIPs[6], network1.subnetMask, network1.hostIPs[0], "client6")
    client7 = sim.createClient("MACADDRESS7", network1.hostIPs[7], network1.subnetMask, network1.hostIPs[0], "client7")
    client8 = sim.createClient("MACADDRESS8", network1.hostIPs[8], network1.subnetMask, network1.hostIPs[0], "client8")
    client9 = sim.createClient("MACADDRESS9", network1.hostIPs[9], network1.subnetMask, network1.hostIPs[0], "client9")
    client10 = sim.createClient("MACADDRESS10", network1.hostIPs[10], network1.subnetMask, network1.hostIPs[0], "client10")

    linka = sim.createLink(client1, switch1, linkType="basic-basic")
    linkb = sim.createLink(client2, switch1, linkType="basic-basic")
    linkc = sim.createLink(client3, switch1, linkType="basic-basic")
    linkd = sim.createLink(client4, switch1, linkType="basic-basic")
    linkf = sim.createLink(client5, switch1, linkType="basic-basic")
    linkg = sim.createLink(client6, switch1, linkType="basic-basic")
    linkh = sim.createLink(client7, switch1, linkType="basic-basic")
    linki = sim.createLink(client8, switch1, linkType="basic-basic")
    linkj = sim.createLink(client9, switch1, linkType="basic-basic")
    linkk = sim.createLink(client10, switch1, linkType="basic-basic")

    #Positions
    return[ [    client2,    [460,100]     ],
            [    client1,    [300,100]     ],
            [    switch1,    [390,300]     ],
            [    client3,    [560,400]     ],
            [    client4,    [450,470]     ],
            [    client5,    [210,400]     ],
            [    client6,    [560,190]     ],
            [    client7,    [210,190]     ],
            [    client8,    [180,300]     ],
            [    client9,    [310,470]     ],
            [    client10,   [590,300]     ]
          ]








def bus():
    sim.reset()
    network1 = sim.createNetwork("100.100", 3)
    T_connect_1 = sim.createHub("T_connect_1")
    T_connect_2 = sim.createHub("T_connect_2")
    T_connect_3 = sim.createHub("T_connect_3")
    T_connect_4 = sim.createHub("T_connect_4")
    client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")
    client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    client3 = sim.createClient("MACADDRESS3", network1.hostIPs[3], network1.subnetMask, network1.hostIPs[0], "client3")
    client4 = sim.createClient("MACADDRESS4", network1.hostIPs[4], network1.subnetMask, network1.hostIPs[0], "client4")
    client5 = sim.createClient("MACADDRESS5", network1.hostIPs[5], network1.subnetMask, network1.hostIPs[0], "client5")
    client6 = sim.createClient("MACADDRESS6", network1.hostIPs[6], network1.subnetMask, network1.hostIPs[0], "client6")

    linka = sim.createLink(T_connect_1, T_connect_2, linkType="basic-basic")
    linkb = sim.createLink(T_connect_2, T_connect_3, linkType="basic-basic")
    linkc = sim.createLink(T_connect_3, T_connect_4, linkType="basic-basic")
    linke = sim.createLink(client1, T_connect_1, linkType="basic-basic")
    linkf = sim.createLink(client2, T_connect_1, linkType="basic-basic")
    linkg = sim.createLink(client3, T_connect_2, linkType="basic-basic")
    linkh = sim.createLink(client4, T_connect_3, linkType="basic-basic")
    linki = sim.createLink(client5, T_connect_4, linkType="basic-basic")
    linkj = sim.createLink(client6, T_connect_4, linkType="basic-basic")


    #Positions
    return[ [T_connect_1,   [272.0, 268.0]      ],
            [client2,       [167.0, 268.0]      ],
            [client3,       [386.0, 364.0]      ],
            [client1,       [271.0, 365.0]      ],
            [T_connect_4,   [601.0, 264.0]      ],
            [T_connect_2,   [385.0, 266.0]      ],
            [T_connect_3,   [492.0, 265.0]      ],
            [client4,       [493.0, 363.0]      ],
            [client6,       [602.0, 366.0]      ],
            [client5,       [705.0, 264.0]      ]
          ]









def pat():
    sim.reset()
    network1 = sim.createNetwork("10.0", 2, "200.200.200.200")        #create private network: size /(30-2), private prefix 10.0, public alias 200.200.200.200
    router1 = sim.createRouter("MACADDRESS0", None, "router1")
    client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")
    client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    client3 = sim.createClient("MACADDRESS3", network1.hostIPs[3], network1.subnetMask, network1.hostIPs[0], "client3")
    switch1 = sim.createSwitch("switch1")
    link1 = sim.createLink(router1, switch1, IPAddress_Router1="200.200.200.200", subnetMask="255.255.255.254", linkType="router-basic")
    sim.nodeDict[router1].link_NAT[link1] = sim.PAT(network1.hostIPs[0], "200.200.200.200", sim.nodeDict[router1].simID)                    #add PAT to router interface
    linka = sim.createLink(client1, switch1, linkType="basic-basic")
    linkb = sim.createLink(client2, switch1, linkType="basic-basic")
    linkc = sim.createLink(client3, switch1, linkType="basic-basic")

    network2 = sim.createNetwork("10.0", 2, "200.200.200.123")        #create private network: size /(30-2), private prefix 10.0, public alias 200.200.200.123
    client4 = sim.createClient("MACADDRESS1", network2.hostIPs[1], network2.subnetMask, network2.hostIPs[0], "client4")
    client5 = sim.createClient("MACADDRESS2", network2.hostIPs[2], network2.subnetMask, network2.hostIPs[0], "client5")
    client6 = sim.createClient("MACADDRESS3", network2.hostIPs[3], network2.subnetMask, network2.hostIPs[0], "client6")
    switch2 = sim.createSwitch("switch2")
    link2 = sim.createLink(router1, switch2, IPAddress_Router1="200.200.200.123", subnetMask="255.255.255.254", linkType="router-basic")
    sim.nodeDict[router1].link_NAT[link2] = sim.PAT(network2.hostIPs[0], "200.200.200.123", sim.nodeDict[router1].simID)                    #add PAT to router interface
    linka = sim.createLink(client4, switch2, linkType="basic-basic")
    linkb = sim.createLink(client5, switch2, linkType="basic-basic")
    linkc = sim.createLink(client6, switch2, linkType="basic-basic")

    router2 = sim.createRouter("MACADDRESS4", None, "router2")
    network3 = sim.createNetwork("100.100", 1)                        #create public network: prefix 100.100, size /(30-1)
    network4 = sim.createNetwork("100.100", 1)                        #create public network: prefix 100.100, size /(30-1)
    network5 = sim.createNetwork("100.100", 1)                        #create public network: prefix 100.100, size /(30-1)
    client7 = sim.createClient("MACADDRESS4", network3.hostIPs[1], network3.subnetMask, network3.hostIPs[0], "client7")
    client8 = sim.createClient("MACADDRESS5", network4.hostIPs[1], network4.subnetMask, network4.hostIPs[0], "client8")
    client9 = sim.createClient("MACADDRESS6", network5.hostIPs[1], network5.subnetMask, network5.hostIPs[0], "client9")
    link3 = sim.createLink(router2, client7, IPAddress_Router1=network3.hostIPs[0], subnetMask=network3.subnetMask, linkType="router-basic")
    link4 = sim.createLink(router2, client8, IPAddress_Router1=network4.hostIPs[0], subnetMask=network4.subnetMask, linkType="router-basic")
    link5 = sim.createLink(router2, client9, IPAddress_Router1=network5.hostIPs[0], subnetMask=network5.subnetMask, linkType="router-basic")
    link6 = sim.createLink(router1, router2, IPAddress_Router1="100.200.210.200", IPAddress_Router2="100.200.211.200", linkType="router-router")

    #Positions
    return[ [client1, [286.0, 99.0]],
            [client2, [372.0, 144.0]],
            [client3, [211.0, 145.0]],
            [client4, [141.0, 359.0]],
            [client5, [89.0, 283.0]],
            [client6, [140.0, 201.0]],
            [client7, [652.0, 362.0]],
            [client8, [584.0, 254.0]],
            [client9, [579.0, 464.0]],
            [router1, [291.0, 284.0]],
            [router2, [520.0, 361.0]],
            [switch1, [289.0, 193.0]],
            [switch2, [193.0, 281.0]]
      ]









def multiNetwork():
    sim.reset()
    network1 = sim.createNetwork("120.36", 2)
    router1 = sim.createRouter("MACADDRESS0", None, "router1")
    client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")
    client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    client3 = sim.createClient("MACADDRESS3", network1.hostIPs[3], network1.subnetMask, network1.hostIPs[0], "client3")
    switch1 = sim.createSwitch("switch1")
    link1 = sim.createLink(router1, switch1, IPAddress_Router1= network1.hostIPs[0], subnetMask=network1.subnetMask, linkType="router-basic")
    linka = sim.createLink(client1, switch1, linkType="basic-basic")
    linkb = sim.createLink(client2, switch1, linkType="basic-basic")
    linkc = sim.createLink(client3, switch1, linkType="basic-basic")

    router2 = sim.createRouter("MACADDRESS4", None, "router2")
    network2 = sim.createNetwork("100.100", 1)
    network3 = sim.createNetwork("100.100", 1)
    network4 = sim.createNetwork("100.100", 1)
    client4 = sim.createClient("MACADDRESS4", network2.hostIPs[1], network2.subnetMask, network2.hostIPs[0], "client4")
    client5 = sim.createClient("MACADDRESS5", network3.hostIPs[1], network3.subnetMask, network3.hostIPs[0], "client5")
    client6 = sim.createClient("MACADDRESS6", network4.hostIPs[1], network4.subnetMask, network4.hostIPs[0], "client6")
    link2 = sim.createLink(router2, client4, IPAddress_Router1=network2.hostIPs[0], subnetMask=network2.subnetMask, linkType="router-basic")
    link3 = sim.createLink(router2, client5, IPAddress_Router1=network3.hostIPs[0], subnetMask=network3.subnetMask, linkType="router-basic")
    link4 = sim.createLink(router2, client6, IPAddress_Router1=network4.hostIPs[0], subnetMask=network4.subnetMask, linkType="router-basic")
    link5 = sim.createLink(router1, router2, IPAddress_Router1="100.200.210.200", IPAddress_Router2="100.200.211.200", linkType="router-router")

    #Positions
    return[ [client1, [286.0, 99.0]],
            [client2, [372.0, 144.0]],
            [client3, [211.0, 145.0]],
            [client4, [652.0, 362.0]],
            [client5, [584.0, 254.0]],
            [client6, [579.0, 464.0]],
            [router1, [291.0, 284.0]],
            [router2, [520.0, 361.0]],
            [switch1, [289.0, 193.0]]
      ]









def multiNetworkBig():
    sim.reset()
    network1 = sim.createNetwork("100.100", 1)
    router1 = sim.createRouter("MACADDRESS0", None, "router1")
    hub1 = sim.createHub("hub1")
    client1 = sim.createClient("MACADDRESS1", network1.hostIPs[1], network1.subnetMask, network1.hostIPs[0], "client1")   #1
    client2 = sim.createClient("MACADDRESS2", network1.hostIPs[2], network1.subnetMask, network1.hostIPs[0], "client2")
    link1 = sim.createLink(router1, hub1, IPAddress_Router1=network1.hostIPs[0], subnetMask=network1.subnetMask, linkType="router-basic")
    link2 = sim.createLink(client1, hub1, linkType="basic-basic")
    link3 = sim.createLink(client2, hub1, linkType="basic-basic")
    #-----------------------------------------------------------------------------------------------------------------------
    network2 = sim.createNetwork("100.100", 1)
    # router2 = sim.createRouter("MACADDRESS3", None, "router2")
    hub2 = sim.createHub("hub2")
    client3 = sim.createClient("MACADDRESS4", network2.hostIPs[1], network2.subnetMask, network2.hostIPs[0], "client3")   #1
    client4 = sim.createClient("MACADDRESS5", network2.hostIPs[2], network2.subnetMask, network2.hostIPs[0], "client4")
    link1 = sim.createLink(router1, hub2, IPAddress_Router1=network2.hostIPs[0], subnetMask=network2.subnetMask, linkType="router-basic")
    link2 = sim.createLink(client3, hub2, linkType="basic-basic")
    link3 = sim.createLink(client4, hub2, linkType="basic-basic")
    #-----------------------------------------------------------------------------------------------------------------------
    network3 = sim.createNetwork("100.100", 1)
    # router3 = sim.createRouter("MACADDRESS6", None, "router3")
    hub3 = sim.createHub("hub3")
    client5 = sim.createClient("MACADDRESS7", network3.hostIPs[1], network3.subnetMask, network3.hostIPs[0], "client5")   #1
    client6 = sim.createClient("MACADDRESS8", network3.hostIPs[2], network3.subnetMask, network3.hostIPs[0], "client6")
    link1 = sim.createLink(router1, hub3, IPAddress_Router1=network3.hostIPs[0], subnetMask=network3.subnetMask, linkType="router-basic")
    link2 = sim.createLink(client5, hub3, linkType="basic-basic")
    link3 = sim.createLink(client6, hub3, linkType="basic-basic")
    #-----------------------------------------------------------------------------------------------------------------------
    network4 = sim.createNetwork("100.100", 1)
    # router4 = sim.createRouter("MACADDRESS9", None, "router4")
    hub4 = sim.createHub("hub4")
    client7 = sim.createClient("MACADDRESS10", network4.hostIPs[1], network4.subnetMask, network4.hostIPs[0], "client7")   #1
    client8 = sim.createClient("MACADDRESS11", network4.hostIPs[2], network4.subnetMask, network4.hostIPs[0], "client8")
    link1 = sim.createLink(router1, hub4, IPAddress_Router1=network4.hostIPs[0], subnetMask=network4.subnetMask, linkType="router-basic")
    link2 = sim.createLink(client7, hub4, linkType="basic-basic")
    link3 = sim.createLink(client8, hub4, linkType="basic-basic")
    #-----------------------------------------------------------------------------------------------------------------------

    network5 = sim.createNetwork("100.100", 4)
    router2 = sim.createRouter("MACADDRESS12", None, "router2")
    switch1 = sim.createSwitch("switch1")
    hub5 = sim.createHub("hub5")
    hub6 = sim.createHub("hub6")
    hub7 = sim.createHub("hub7")
    hub8 = sim.createHub("hub8")
    client9 = sim.createClient("MACADDRESS13", network5.hostIPs[1], network5.subnetMask, network5.hostIPs[0], "client9")   #1
    client10 = sim.createClient("MACADDRESS14", network5.hostIPs[2], network5.subnetMask, network5.hostIPs[0], "client10")
    client11 = sim.createClient("MACADDRESS15", network5.hostIPs[3], network5.subnetMask, network5.hostIPs[0], "client11")
    client12 = sim.createClient("MACADDRESS16", network5.hostIPs[4], network5.subnetMask, network5.hostIPs[0], "client12")
    client13 = sim.createClient("MACADDRESS17", network5.hostIPs[5], network5.subnetMask, network5.hostIPs[0], "client13")
    client14 = sim.createClient("MACADDRESS18", network5.hostIPs[6], network5.subnetMask, network5.hostIPs[0], "client14")
    client15 = sim.createClient("MACADDRESS19", network5.hostIPs[7], network5.subnetMask, network5.hostIPs[0], "client15")   #1
    client16 = sim.createClient("MACADDRESS20", network5.hostIPs[8], network5.subnetMask, network5.hostIPs[0], "client16")
    client17 = sim.createClient("MACADDRESS21", network5.hostIPs[9], network5.subnetMask, network5.hostIPs[0], "client17")
    client18 = sim.createClient("MACADDRESS22", network5.hostIPs[10], network5.subnetMask, network5.hostIPs[0], "client18")
    client19 = sim.createClient("MACADDRESS23", network5.hostIPs[11], network5.subnetMask, network5.hostIPs[0], "client19")
    client20 = sim.createClient("MACADDRESS24", network5.hostIPs[12], network5.subnetMask, network5.hostIPs[0], "client20")
    client21 = sim.createClient("MACADDRESS25", network5.hostIPs[13], network5.subnetMask, network5.hostIPs[0], "client21")
    client22 = sim.createClient("MACADDRESS26", network5.hostIPs[14], network5.subnetMask, network5.hostIPs[0], "client22")
    client23 = sim.createClient("MACADDRESS27", network5.hostIPs[15], network5.subnetMask, network5.hostIPs[0], "client23")
    client24 = sim.createClient("MACADDRESS28", network5.hostIPs[15], network5.subnetMask, network5.hostIPs[0], "client24")
    client25 = sim.createClient("MACADDRESS29", network5.hostIPs[17], network5.subnetMask, network5.hostIPs[0], "client25")
    client26 = sim.createClient("MACADDRESS30", network5.hostIPs[18], network5.subnetMask, network5.hostIPs[0], "client26")
    client27 = sim.createClient("MACADDRESS31", network5.hostIPs[19], network5.subnetMask, network5.hostIPs[0], "client27")
    client28 = sim.createClient("MACADDRESS32", network5.hostIPs[20], network5.subnetMask, network5.hostIPs[0], "client28")
    client29 = sim.createClient("MACADDRESS33", network5.hostIPs[21], network5.subnetMask, network5.hostIPs[0], "client29")
    client30 = sim.createClient("MACADDRESS34", network5.hostIPs[22], network5.subnetMask, network5.hostIPs[0], "client30")
    client31 = sim.createClient("MACADDRESS35", network5.hostIPs[23], network5.subnetMask, network5.hostIPs[0], "client31")
    client32 = sim.createClient("MACADDRESS36", network5.hostIPs[24], network5.subnetMask, network5.hostIPs[0], "client32")

    link1 = sim.createLink(router2, switch1, IPAddress_Router1=network5.hostIPs[0], subnetMask=network5.subnetMask, linkType="router-basic")

    link2 = sim.createLink(client9, hub5, linkType="basic-basic")
    link3 = sim.createLink(client10, hub5, linkType="basic-basic")
    link4 = sim.createLink(client11, hub5, linkType="basic-basic")
    link5 = sim.createLink(client12, hub5, linkType="basic-basic")
    link6 = sim.createLink(client13, hub5, linkType="basic-basic")
    link7 = sim.createLink(client14, hub5, linkType="basic-basic")

    link8 = sim.createLink(client15, hub6, linkType="basic-basic")
    link9 = sim.createLink(client16, hub6, linkType="basic-basic")
    link10 = sim.createLink(client17, hub6, linkType="basic-basic")
    link11 = sim.createLink(client18, hub6, linkType="basic-basic")
    link12 = sim.createLink(client19, hub6, linkType="basic-basic")
    link13 = sim.createLink(client20, hub6, linkType="basic-basic")
    link14 = sim.createLink(client21, hub6, linkType="basic-basic")
    link15 = sim.createLink(client22, hub6, linkType="basic-basic")
    link16 = sim.createLink(client23, hub6, linkType="basic-basic")
    link17 = sim.createLink(client24, hub6, linkType="basic-basic")
    link18 = sim.createLink(client25, hub6, linkType="basic-basic")
    link19 = sim.createLink(client26, hub6, linkType="basic-basic")

    link20 = sim.createLink(client27, hub7, linkType="basic-basic")
    link21 = sim.createLink(client28, hub7, linkType="basic-basic")
    link22 = sim.createLink(client29, hub7, linkType="basic-basic")

    link23 = sim.createLink(client30, hub8, linkType="basic-basic")
    link24 = sim.createLink(client31, hub8, linkType="basic-basic")
    link25 = sim.createLink(client32, hub8, linkType="basic-basic")

    link26 = sim.createLink(switch1, hub5, linkType="basic-basic")
    link27 = sim.createLink(switch1, hub6, linkType="basic-basic")
    link28 = sim.createLink(switch1, hub7, linkType="basic-basic")
    link29 = sim.createLink(switch1, hub8, linkType="basic-basic")

#--------------------------------------------------------------------------------------------------------------------------------

    network5 = sim.createNetwork("100.100", 2)
    router3 = sim.createRouter("MACADDRESS37", None, "router3")
    hub9 = sim.createHub("hub9")
    client33 = sim.createClient("MACADDRESS38", network5.hostIPs[1], network5.subnetMask, network5.hostIPs[0], "client33")
    client34 = sim.createClient("MACADDRESS39", network5.hostIPs[2], network5.subnetMask, network5.hostIPs[0], "client34")   #1
    client35 = sim.createClient("MACADDRESS40", network5.hostIPs[3], network5.subnetMask, network5.hostIPs[0], "client35")
    client36 = sim.createClient("MACADDRESS41", network5.hostIPs[4], network5.subnetMask, network5.hostIPs[0], "client36")   #1
    client37 = sim.createClient("MACADDRESS42", network5.hostIPs[5], network5.subnetMask, network5.hostIPs[0], "client37")
    link1 = sim.createLink(router3, hub9, IPAddress_Router1=network5.hostIPs[0], subnetMask=network5.subnetMask, linkType="router-basic")
    link2 = sim.createLink(client33, hub9, linkType="basic-basic")
    link3 = sim.createLink(client34, hub9, linkType="basic-basic")
    link4 = sim.createLink(client35, hub9, linkType="basic-basic")
    link5 = sim.createLink(client36, hub9, linkType="basic-basic")
    link6 = sim.createLink(client37, hub9, linkType="basic-basic")

    network6 = sim.createNetwork("100.100", 2)
    hub10 = sim.createHub("hub10")
    client38 = sim.createClient("MACADDRESS43", network6.hostIPs[1], network6.subnetMask, network6.hostIPs[0], "client38")   #1
    client39 = sim.createClient("MACADDRESS44", network6.hostIPs[2], network6.subnetMask, network6.hostIPs[0], "client39")
    client40 = sim.createClient("MACADDRESS45", network6.hostIPs[3], network6.subnetMask, network6.hostIPs[0], "client40")   #1
    client41 = sim.createClient("MACADDRESS46", network6.hostIPs[4], network6.subnetMask, network6.hostIPs[0], "client41")
    link1 = sim.createLink(router3, hub10, IPAddress_Router1=network6.hostIPs[0], subnetMask=network6.subnetMask, linkType="router-basic")
    link2 = sim.createLink(client38, hub10, linkType="basic-basic")
    link3 = sim.createLink(client39, hub10, linkType="basic-basic")
    link4 = sim.createLink(client40, hub10, linkType="basic-basic")
    link5 = sim.createLink(client41, hub10, linkType="basic-basic")

    router4 = sim.createRouter("MACADDRESS47", None, "router4")

    #router links
    link1 = sim.createLink(router1, router4, IPAddress_Router1="100.200.210.200", IPAddress_Router2="100.200.210.201", linkType="router-router")
    link2 = sim.createLink(router2, router4, IPAddress_Router1="100.200.210.202", IPAddress_Router2="100.200.210.203", linkType="router-router")
    link3 = sim.createLink(router3, router4, IPAddress_Router1="100.200.210.204", IPAddress_Router2="100.200.210.205", linkType="router-router")
    link4 = sim.createLink(router1, router3, IPAddress_Router1="100.200.210.206", IPAddress_Router2="100.200.210.207", linkType="router-router")
    link5 = sim.createLink(router2, router3, IPAddress_Router1="100.200.210.208", IPAddress_Router2="100.200.210.209", linkType="router-router")

#--------------------------------------------------------------------------------------------------------------------------------

    network7 = sim.createNetwork("100.100", 2)
    router5 = sim.createRouter("MACADDRESS38", None, "router5")
    hub11 = sim.createHub("hub11")
    client42 = sim.createClient("MACADDRESS48", network7.hostIPs[1], network7.subnetMask, network7.hostIPs[0], "client42")   #1
    client43 = sim.createClient("MACADDRESS49", network7.hostIPs[2], network7.subnetMask, network7.hostIPs[0], "client43")
    client44 = sim.createClient("MACADDRESS50", network7.hostIPs[3], network7.subnetMask, network7.hostIPs[0], "client44")   #1
    client45 = sim.createClient("MACADDRESS51", network7.hostIPs[4], network7.subnetMask, network7.hostIPs[0], "client45")
    link1 = sim.createLink(router5, hub11, IPAddress_Router1=network7.hostIPs[0], subnetMask=network7.subnetMask, linkType="router-basic")
    link2 = sim.createLink(client42, hub11, linkType="basic-basic")
    link3 = sim.createLink(client43, hub11, linkType="basic-basic")
    link4 = sim.createLink(client44, hub11, linkType="basic-basic")
    link5 = sim.createLink(client45, hub11, linkType="basic-basic")

    network8 = sim.createNetwork("100.100", 2)
    hub12 = sim.createHub("hub12")
    client46 = sim.createClient("MACADDRESS52", network8.hostIPs[1], network8.subnetMask, network8.hostIPs[0], "client46")   #1
    client47 = sim.createClient("MACADDRESS53", network8.hostIPs[2], network8.subnetMask, network8.hostIPs[0], "client47")
    client48 = sim.createClient("MACADDRESS54", network8.hostIPs[3], network8.subnetMask, network8.hostIPs[0], "client48")   #1
    client49 = sim.createClient("MACADDRESS55", network8.hostIPs[4], network8.subnetMask, network8.hostIPs[0], "client49")
    link1 = sim.createLink(router5, hub12, IPAddress_Router1=network8.hostIPs[0], subnetMask=network8.subnetMask, linkType="router-basic")
    link2 = sim.createLink(client46, hub12, linkType="basic-basic")
    link3 = sim.createLink(client47, hub12, linkType="basic-basic")
    link4 = sim.createLink(client48, hub12, linkType="basic-basic")
    link5 = sim.createLink(client49, hub12, linkType="basic-basic")


    network9 = sim.createNetwork("100.100", 3)
    switch2 = sim.createSwitch("switch2")
    link1 = sim.createLink(router5, switch2, IPAddress_Router1=network9.hostIPs[0], subnetMask=network9.subnetMask, linkType="router-basic")

    hub13 = sim.createHub("hub13")
    client50 = sim.createClient("MACADDRESS56", network9.hostIPs[1], network9.subnetMask, network9.hostIPs[0], "client50")   #1
    client51 = sim.createClient("MACADDRESS57", network9.hostIPs[2], network9.subnetMask, network9.hostIPs[0], "client51")
    client52 = sim.createClient("MACADDRESS58", network9.hostIPs[3], network9.subnetMask, network9.hostIPs[0], "client52")   #1
    client53 = sim.createClient("MACADDRESS59", network9.hostIPs[4], network9.subnetMask, network9.hostIPs[0], "client53")
    client54 = sim.createClient("MACADDRESS60", network9.hostIPs[5], network9.subnetMask, network9.hostIPs[0], "client54")   #1
    client55 = sim.createClient("MACADDRESS61", network9.hostIPs[6], network9.subnetMask, network9.hostIPs[0], "client55")
    client56 = sim.createClient("MACADDRESS62", network9.hostIPs[7], network9.subnetMask, network9.hostIPs[0], "client56")   #1
    client57 = sim.createClient("MACADDRESS63", network9.hostIPs[8], network9.subnetMask, network9.hostIPs[0], "client57")

    link1 = sim.createLink(switch2, hub13, linkType="basic-basic")
    link2 = sim.createLink(client50, hub13, linkType="basic-basic")
    link3 = sim.createLink(client51, hub13, linkType="basic-basic")
    link4 = sim.createLink(client52, hub13, linkType="basic-basic")
    link5 = sim.createLink(client53, hub13, linkType="basic-basic")
    link2 = sim.createLink(client54, hub13, linkType="basic-basic")
    link3 = sim.createLink(client55, hub13, linkType="basic-basic")
    link4 = sim.createLink(client56, hub13, linkType="basic-basic")
    link5 = sim.createLink(client57, hub13, linkType="basic-basic")

    hub14 = sim.createHub("hub14")
    client58 = sim.createClient("MACADDRESS64", network9.hostIPs[9], network9.subnetMask, network9.hostIPs[0], "client58")   #1
    client59 = sim.createClient("MACADDRESS65", network9.hostIPs[10], network9.subnetMask, network9.hostIPs[0], "client59")
    client60 = sim.createClient("MACADDRESS66", network9.hostIPs[11], network9.subnetMask, network9.hostIPs[0], "client60")   #1
    link1 = sim.createLink(switch2, hub14, linkType="basic-basic")
    link2 = sim.createLink(client58, hub14, linkType="basic-basic")
    link3 = sim.createLink(client59, hub14, linkType="basic-basic")
    link4 = sim.createLink(client60, hub14, linkType="basic-basic")


    #-----------------------------------------------------------------------------------------------------------------------
    cloud = sim.createRouter("MACADDRESS67", None, "cloud")
    #-----------------------------------------------------------------------------------------------------------------------
    link1 = sim.createLink(cloud, router5, IPAddress_Router1="100.200.210.210", IPAddress_Router2="100.200.210.211", linkType="router-router")
    link2 = sim.createLink(cloud, router4, IPAddress_Router1="100.200.210.212", IPAddress_Router2="100.200.210.213", linkType="router-router")
    #Positions
    return[ [client1, [470.1228882668983, 565.0]],
            [client10, [1892.960917456862, 587.2566890217533]],
            [client11, [1809.960917456862, 685.2566890217533]],
            [client12, [1845.960917456862, 454.2566890217533]],
            [client13, [1784.960917456862, 400.2566890217533]],
            [client14, [1860.960917456862, 636.2566890217533]],
            [client15, [1558.0, 178.7783940834879]],
            [client16, [1396.0, 143.7783940834879]],
            [client17, [1295.0, 206.0370110712551]],
            [client18, [1338.0, 169.0370110712551]],
            [client19, [1465.0, 430.7783940834879]],
            [client2, [482.6228882668983, 620.0]],
            [client20, [1506.0, 146.7783940834879]],
            [client21, [1602.0, 270.7783940834879]],
            [client22, [1446.0, 139.7783940834879]],
            [client23, [1516.0, 410.7783940834879]],
            [client24, [1588.0, 224.7783940834879]],
            [client25, [1597.0, 334.7783940834879]],
            [client26, [1559.0, 379.7783940834879]],
            [client27, [1571.1693449151626, 880.7795882087073]],
            [client28, [1520.1693449151626, 897.7795882087073]],
            [client29, [1610.1693449151626, 837.7795882087073]],
            [client3, [345.1228882668984, 375.0]],
            [client30, [1573.1834390855663, 673.8337202634452]],
            [client31, [1521.1834390855663, 688.8337202634452]],
            [client32, [1595.1834390855663, 617.8337202634452]],
            [client33, [1175.0, 130.0]],
            [client34, [1135.0, 105.0]],
            [client35, [1197.5, 170.0]],
            [client36, [1087.5, 97.5]],
            [client37, [1040.0, 125.0]],
            [client38, [910.0, 225.0]],
            [client39, [962.5, 212.5]],
            [client4, [337.6228882668984, 435.0]],
            [client40, [880.0, 267.5]],
            [client41, [1015.0, 227.5]],
            [client42, [545.1228882668984, 1362.5]],
            [client43, [512.6228882668984, 1315.0]],
            [client44, [497.62288826689837, 1202.5]],
            [client45, [487.62288826689837, 1257.5]],
            [client46, [1476.6228882668984, 1372.5]],
            [client47, [1519.1228882668984, 1225.0]],
            [client48, [1521.6228882668984, 1332.5]],
            [client49, [1531.6228882668984, 1275.0]],
            [client5, [597.6228882668983, 332.5]],
            [client50, [1085.1228882668984, 1528.1535816818537]],
            [client51, [1037.6228882668984, 1525.0]],
            [client52, [1195.1228882668984, 1492.5]],
            [client53, [1232.6228882668984, 1460.0]],
            [client54, [1247.6228882668984, 1412.5]],
            [client55, [1245.1228882668984, 1367.5]],
            [client56, [1142.6228882668984, 1517.5]],
            [client57, [997.6228882668984, 1502.5]],
            [client58, [870.1228882668984, 1450.0]],
            [client59, [795.1228882668984, 1390.0]],
            [client6, [630.1228882668983, 282.5]],
            [client60, [815.1228882668984, 1432.5]],
            [client7, [650.1228882668983, 90.0]],
            [client8, [610.1228882668984, 127.5]],
            [client9, [1885.960917456862, 519.2566890217533]],
            [hub1, [580.1228882668984, 585.0]],
            [hub10, [962.5, 290.0]],
            [hub11, [645.1228882668983, 1242.5]],
            [hub12, [1380.1228882668984, 1267.5]],
            [hub13, [1045.1228882668984, 1357.5]],
            [hub14, [897.6228882668984, 1352.5]],
            [hub2, [432.6228882668983, 430.0]],
            [hub3, [690.1228882668983, 352.5]],
            [hub4, [702.6228882668983, 177.5]],
            [hub5, [1703.960917456862, 549.2566890217533]],
            [hub6, [1395.0, 307.0370110712551]],
            [hub7, [1515.960917456862, 804.1598239743837]],
            [hub8, [1484.960917456862, 585.1598239743837]],
            [hub9, [1110.0, 170.30452638879626]],
            [router1, [967.6228882668983, 525.0]],
            [router2, [1107.6228882668984, 525.0]],
            [router3, [1032.6228882668984, 487.5]],
            [router4, [1035.1228882668984, 565.0]],
            [router5, [995.1228882668983, 1102.5]],
            [switch1, [1315.0, 485.5]],
            [switch2, [987.6228882668983, 1285.0]],
            [cloud, [932.6228882668983, 835.0]]
      ]

