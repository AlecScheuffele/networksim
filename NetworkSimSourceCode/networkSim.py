import string
import random
import copy

#TODO:  close connection (FIN)
#TODO:  choose file to download.
#TODO:  add tcp error detection
#TODO:  Message out can be inherited from Node  -- but type attribute will need to be added for print to work
#TODO:  remove objects (nodes/links)  --- but ~not in scope of this proj

#before pres
#TODO: send file with TCP handshake.  maybe add error detection
#TODO: ad the IP of default gateway (and maybe every other network layer prep-message)
#TODO: add step-by() method to make multiple steps.  can aid in simulating timeouts







####################################################################################################################################################
####################################################################################################################################################
#model
####################################################################################################################################################
####################################################################################################################################################
nodeDict = {}
links = {}
addressPrefixSpace = {}
networks = {}
stepCount = [0]
stepHistory = {}  #   step -------> [nodeDict,links,environment]
consoleOut = [[]]
IPSet = set()
packetDropRate = 0.5








####################################################################################################################################################
####################################################################################################################################################
#class heirarchy
####################################################################################################################################################
####################################################################################################################################################

class Link(object):

    def __str__(self):
        return self.getInfo()

    def __init__(self, simID_DeviceA, simID_DeviceB):
        self.simID = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
        self.simID_DeviceA = simID_DeviceA
        self.simID_DeviceB = simID_DeviceB
        self.message_GoingTo_Point_A = []
        self.message_GoingTo_Point_B = []
        #------
        self.message_ArrivedAt_Point_A = []
        self.message_ArrivedAt_Point_B = []

    def getInfo(self):
        result = self.simID + ":" + str('%+15s' % (nodeDict[self.simID_DeviceA].name)) + " <--------------------------------> " + str('%-15s' % (nodeDict[self.simID_DeviceB].name))
        for message in self.message_GoingTo_Point_A:
            result = result + "\n\n" + message.getInfo(14)
        for message in self.message_GoingTo_Point_B:
            result = result + "\n\n" + message.getInfo(14)
        if len(self.message_GoingTo_Point_A) > 0 or len(self.message_GoingTo_Point_B) > 0:
            result = result + "\n\n"
        return result


    def transfer(self):
        self.message_ArrivedAt_Point_A = self.message_GoingTo_Point_A
        self.message_ArrivedAt_Point_B = self.message_GoingTo_Point_B
        self.message_GoingTo_Point_A = []
        self.message_GoingTo_Point_B = []

    def send(self, sender_simID, message):
        if sender_simID == self.simID_DeviceA:
            self.message_GoingTo_Point_B.append(message)
        elif sender_simID == self.simID_DeviceB:
            self.message_GoingTo_Point_A.append(message)
        else:
            consoleRec("err. " + sender_simID + " not linked")

    def read(self, reader_simID):
        if reader_simID == self.simID_DeviceA:
            messages = self.message_ArrivedAt_Point_A
            self.message_ArrivedAt_Point_A = []
            return messages
        elif reader_simID == self.simID_DeviceB:
            messages = self.message_ArrivedAt_Point_B
            self.message_ArrivedAt_Point_B = []
            return messages
        else:
            consoleRec("err. " + reader_simID + " not linked")


class Network(object):
    def __init__(self, networkID, subnetMask, broadcastAddress, hostIPs):
        self.networkID = networkID
        self.broadcastAddress = broadcastAddress
        self.subnetMask = subnetMask
        self.hostIPs = hostIPs

    def __str__(self):
        return self.getInfo()

    def getInfo(self):

        self.stdInfo = "".join([str("\n" + '%-12s%-2s%-16s' % (x, "", y)) for x, y in zip(["Network ID:", "Range:", "Subnet Mask:", "total IPs:"], [self.networkID, self.networkID + "  --->  " + self.broadcastAddress, self.subnetMask, str(len(self.hostIPs)+2)])])
        return self.stdInfo + "\n" + "-----------------------------------------------------------------"


#--------------------------------------------
#--------------------------------------------


class Node(object):
    def __init__(self, name):
        self.simID = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
        self.name = name
        self.linkSet = set()
        self.type = "unassigned"

    def __str__(self):
        return self.getInfo()

    def getInfo(self):
        return "nothing implemented yet.  this s a super class"

    def outputOntoLink(message):
        return "nothing implemented yet.  this s a super class"

    def recieveMessage(self):
        for alink in self.linkSet:
            incommingMessages = links[alink].read(self.simID)
            for message in incommingMessages:
                self.processMessage(message, alink)

    def processMessage(self):
        return "nothing implemented yet.  this s a super class"


class Client(Node):
    def __init__(self, macAddress, IPAddress, subnetMask, defaultGateway, name):
        Node.__init__(self, name)
        self.macAddress = macAddress
        self.IPAddress = IPAddress
        self.subnetMask = subnetMask
        self.lookupTable_MAC = {}
        self.messageLimbo = {}                                                      # destinationIP -> [message1, message2....]
        self.defaultGateway = defaultGateway
        self.type = "CLIENT"
        self.transport = Transport(self.simID)
        self.applications = {}
        self.applications["1500"] = ClientApplication(self.simID, appType="client", data=None, portNumber=1500)
        self.applications["2500"] = ServerApplication(self.simID, appType="server", data=None, portNumber=2500)
        #self.macAddress = macAddress

    def getInfo(self):
        lookupTableString = str('%-35s' % ("empty")) if len(self.lookupTable_MAC) is 0 else "".join([str("\n" + '%-22s%-18s%-8s%-18s' % ("", y[0], y[1], y[2])) for y in [["IP:", "", "MAC Address:"]] + [[x, "--->", self.lookupTable_MAC[x]] for x in self.lookupTable_MAC]])
        stdInfo = "".join([str("\n" + '%-0s%-16s%-6s%-16s' % ("", x, "", y)) for x, y in zip(["name:", "simID:", "MAC address:", "subnetMask", "defaultGateway:", "IP Address:", "lookupTable:"], [self.name, self.simID, self.macAddress, self.subnetMask, self.defaultGateway, self.IPAddress, lookupTableString])])
        return "-----------------------------------CLIENT-----------------------------------" + stdInfo

    def outputOntoLink(self, message, port):
        links[port].send(self.simID, message)
        consoleRec("Client " + self.name + " send message" + " on link " + port)

    def recieveMessage(self):                                                       #currently only supports 1 in/out interface
        for alink in self.linkSet:
            incommingMessages = links[alink].read(self.simID)
            for message in incommingMessages:
                self.processMessage(message, alink)

    def processMessage(self, message, simID_link):
        if (message.destinationMAC == "FF-FF-FF-FF-FF-FF") or (message.destinationMAC == self.macAddress):
            if message.protocolType == "ArpRequest":
                packet = message.payload
                segment = packet.payload
                if packet.destinationIP == self.IPAddress:
                    consoleRec("CLIENT " + self.name + " got ARP request!")
                    self.lookupTable_MAC[packet.sourceIP] = message.sourceMAC
                    self.respondToArp(packet.sourceIP, message.sourceMAC, segment, simID_link)
                else:
                    consoleRec("CLIENT " + self.name + " got ARP request ---WRONG DESTINATION IP---   --- IGNORING ---")
            elif message.protocolType == "ArpResponse":
                if message.destinationMAC == self.macAddress:
                    #Receive Arp Response
                    packet = message.payload
                    segment = packet.payload
                    self.lookupTable_MAC[packet.sourceIP] = message.sourceMAC
                    consoleRec("CLIENT " + self.name + " got ARP response! --- added " + packet.sourceIP + "--->" + message.sourceMAC + " to ARP lookup table")
                    if packet.sourceIP in self.messageLimbo:
                        consoleRec("Releasing messages with destination " + packet.sourceIP  + " from messageLimbo")
                        originalJobs = self.messageLimbo.pop(packet.sourceIP, [])  #pop original messages/links from message limbo
                        for job in originalJobs:                                   #send original messages on original links
                            self.prepareMessage(job[0], job[1])

            elif message.protocolType == "default":
                consoleRec("CLIENT " + self.name + " received its " + message.payload.protocolType + "message")
                if message.payload.protocolType == "get_resp":
                    consoleRec("CLIENT " + self.name + " sending response")
                    packet = message.payload
                    segment = packet.payload
                    self.prepareMessage(Packet(packet.destinationIP, packet.sourceIP, Segment(segment.destinationPort, segment.sourcePort)), simID_link)
                elif message.payload.protocolType == "TCP":
                    packet = message.payload
                    self.transport.handle(packet)

    def respondToArp(self, destinationIP, destinationMAC, segment, simID_link):
        message = ArpResponseTEST(self.IPAddress, destinationIP, self.macAddress, destinationMAC, segment)
        consoleRec("CLIENT " + self.name + " Constructed ARP response")
        self.outputOntoLink(message, simID_link)


    def check_isDestinationInSubnet(self, destinationIP):
        destinationIP_Bytes = [int(x) for x in destinationIP.split(".")]
        thisIP_Bytes = [int(x) for x in self.IPAddress.split(".")]
        subnetMask_Bytes = [int(x) for x in self.subnetMask.split(".")]
        result = True
        for i in range(0, len(destinationIP_Bytes)):
            if not ((destinationIP_Bytes[i] & subnetMask_Bytes[i]) is (thisIP_Bytes[i] & subnetMask_Bytes[i])):
                result = False
        return(result)


    def prepareMessage(self, packet, link):
        if self.check_isDestinationInSubnet(packet.destinationIP):              #On the same Subnet
            consoleRec("destination is in subnet.")
            if packet.destinationIP in self.lookupTable_MAC:
                destinationMAC = self.lookupTable_MAC[packet.destinationIP]
                message = Frame(self.macAddress, destinationMAC, packet)
                self.outputOntoLink(message, link)
            else:
                consoleRec("CLIENT " + self.name + " no mac-address stored for destinationIP, preparing ARP")
                self.sendArp(packet.destinationIP, link, packet)
        else:
            consoleRec("NOT ON SUBNET!!!!!!!!!!!!! sending to default gateway")
            if self.defaultGateway in self.lookupTable_MAC:
                destinationMAC = self.lookupTable_MAC[self.defaultGateway]
                message = Frame(self.macAddress, destinationMAC, packet)
                self.outputOntoLink(message, link)
            else:
                consoleRec("CLIENT " + self.name + " no mac-address stored for destinationIP, preparing ARP")
                self.sendArp(self.defaultGateway, link, packet)


    def sendArp(self, destinationIP, link, originalPacket=None):                                                 #send ARP message and add original packet to messageLimbo
        arpMessage = ArpRequestTEST(self.IPAddress, destinationIP, self.macAddress)
        if not(originalPacket is None):
            consoleRec("adding parent message to messageLimbo")
            if destinationIP in self.messageLimbo:
                self.messageLimbo[destinationIP].append([originalPacket, link])
            else:
                self.messageLimbo[destinationIP] = [[originalPacket, link]]
        consoleRec("Sending ARP")
        self.outputOntoLink(arpMessage, link)

class Hub(Node):
    def __init__(self, name):
        Node.__init__(self, name)
        self.type = "HUB"

    def getInfo(self):
        stdInfo = "".join([str("\n" + '%-0s%-16s%-6s%-16s' % ("", x, "", y)) for x, y in zip(["name:", "simID:"], [self.name, self.simID])])
        return "-----------------------------------HUB-----------------------------------" + stdInfo

    def outputOntoLink(self, message, port):
        links[port].send(self.simID, message)
        consoleRec("Hub " + self.name + " relayed message on link " + port)


    def processMessage(self, message, from_link):
        consoleRec("Hub " + self.name + " processing message")
        for link_simID in self.linkSet:
            if link_simID != from_link:
                self.outputOntoLink(message, link_simID)


class Switch(Node):
    def __init__(self, name):
        Node.__init__(self, name)
        self.forwardingTable = {}
        self.type = "SWITCH"

    def getInfo(self):
        lookupTableString = str('%-22s' % ("empty")) if len(self.forwardingTable) is 0 else "".join([str("\n" + '%-22s%-18s%-8s%-18s' % ("", y[0], y[1], y[2])) for y in [["MAC Address:", "", "Link:"]] + [[x, "--->", self.forwardingTable[x]] for x in self.forwardingTable]])
        #linksString = str('%-35s' % ("empty")) if len(self.linkSet) is 0 else "".join([str("\n" + '%-35s%-0s' % ("", y)) for y in self.linkSet])
        stdInfo = "".join([str("\n" + '%-0s%-16s%-6s%-16s' % ("", x, "", y)) for x, y in zip(["name:", "simID:", "forwardingTable:"], [self.name, self.simID, lookupTableString])])
        return "-----------------------------------SWITCH-----------------------------------" + stdInfo

                                                                                            #physical...
    def outputOntoLink(self, message, port):
        links[port].send(self.simID, message)
        consoleRec("Switch " + self.name + " relayed message on link " + port)


    def processMessage(self, message, from_link):
        consoleRec("Switch " + self.name + " processing message")
        self.updateForwardingTable(message.sourceMAC, from_link)                            #refresh forwarding table
        if message.destinationMAC in self.forwardingTable:                                  #direct message if forwarding table contains destinationMac
            consoleRec("Switch " + self.name + " recognized MAC in forwarding table")
            self.outputOntoLink(message, self.forwardingTable[message.destinationMAC])
        else:                                                                                                  #broadcast message if destinationMAC not in table
            if message.destinationMAC is "FF-FF-FF-FF-FF-FF":
                consoleRec("Switch " + self.name + " RECIEVED Broadcast message --- broadcasting message")     #broadcasted because message was ARP
                self.broadcastMessage(message, from_link)
            else:
                consoleRec("Switch " + self.name + " MAC ---UNRECOGNIZED--- broadcasting message")      #broadcasted because unrecognized MAC
                self.broadcastMessage(message, from_link)

    def broadcastMessage(self, message, from_link):
        for link_simID in self.linkSet:
                if link_simID != from_link:
                    self.outputOntoLink(message, link_simID)

    def updateForwardingTable(self, sourceMAC, from_link):                                              #add sourceMac to update table (if it isnt there already)
        if not (sourceMAC in self.forwardingTable):
            consoleRec("Switch " + self.name + " Adding new Entry " + sourceMAC + "--->" + from_link + " to forwarding table")
            self.forwardingTable[sourceMAC] = from_link


class Router(Node):
    def __init__(self, macAddress, defaultGateway, name):
        Node.__init__(self, name)
        self.macAddress = macAddress
        self.lookupTable_MAC = {}               #destinationIP -> macAddress
        self.link_IP = {}                       #link -> [selfIP, hopIP]         ---hopIP is "NONE" if link is on LAN---
        self.IP_Link = {}                       #selfIP -> link
        self.routingTable = {}                  #routing table:  networkID -> [networkID, subnetMask, outgoingLink, hops]
        self.defaultGateway = defaultGateway    #The default exit IP if the router
        self.messageLimbo = {}
        self.type = "ROUTER"
        self.link_NAT = {}                      #link -> NAT


    def getInfo(self):
        routingTableString = "".join([str("\n" + '%-22s%-16s%-18s%-18s%-8s' % ("", y[0], y[1], y[2], str(y[3]))) for y in [["NetworkID", "SubnetMask", "Interface(link)", "hops"]] + [self.routingTable[x] for x in self.routingTable]])
        lookupTableString = str('%-22s' % ("empty")) if len(self.lookupTable_MAC) is 0 else "".join([str("\n" + '%-22s%-18s%-8s%-18s' % ("", y[0], y[1], y[2])) for y in [["IP:", "", "MAC Address:"]] + [[x, "--->", self.lookupTable_MAC[x]] for x in self.lookupTable_MAC]])
        IP_LinksString = "".join([str("\n" + '%-22s%-16s%-19s%-18s' % ("", y[0], y[1], y[2])) for y in [["Link:", "IP_near:", "IP_far:"]] + [[x, self.link_IP[x][0], self.link_IP[x][1]] for x in self.link_IP]])
        stdInfo = "".join([str("\n" + '%-0s%-16s%-6s%-16s' % ("", x, "", y)) for x, y in zip(["name:", "simID:", "MAC address:", "defaultGateway:", "IP_Links:", "lookupTable:", "routingTable:"], [self.name, self.simID, self.macAddress, self.defaultGateway, IP_LinksString, lookupTableString, routingTableString])])
        return "-----------------------------------ROUTER----------------------------------" + "\n" + stdInfo


    def outputOntoLink(self, message, port):
        links[port].send(self.simID, message)
        consoleRec("Router " + self.name + " send message on link " + port)


    def processMessage(self, message, simID_link):
        if (message.destinationMAC == "FF-FF-FF-FF-FF-FF") or (message.destinationMAC == self.macAddress):                                                 #listen to message
            if message.protocolType == "ArpRequest":
                packet = message.payload
                segment = packet.payload
                if packet.destinationIP == self.link_IP[simID_link][0]:
                    consoleRec("ROUTER " + self.name + " got ARP request!")                                                                             #Public LAN
                    self.lookupTable_MAC[packet.sourceIP] = message.sourceMAC
                    self.respondToArp(packet.sourceIP, message.sourceMAC, self.link_IP[simID_link][0], self.macAddress, segment, simID_link)
                elif len(self.link_NAT) > 0 and packet.destinationIP == self.link_NAT[simID_link].privateIP:                                            #Private LAN
                    consoleRec("ROUTER " + self.name + " got ARP request!")
                    self.link_NAT[simID_link].lookupTable_MAC[packet.sourceIP] = message.sourceMAC
                    self.respondToArp(packet.sourceIP, message.sourceMAC, self.link_NAT[simID_link].privateIP, self.macAddress, segment, simID_link)
                else:
                    consoleRec("ROUTER " + self.name + " sniffed ARP ---WRONG DESTINATION IP---   --- IGNORING ---")
            elif message.protocolType == "ArpResponse":
                if message.destinationMAC == self.macAddress:
                    packet = message.payload
                    segment = packet.payload
                    self.lookupTable_MAC[packet.sourceIP] = message.sourceMAC
                    consoleRec("ROUTER " + self.name + " got ARP response! --- added " + packet.sourceIP + "--->" + message.sourceMAC + " to ARP lookup table")
                    if packet.sourceIP in self.messageLimbo:
                        consoleRec("Releasing messages with destination " + packet.sourceIP  + " from messageLimbo")
                        originalJobs = self.messageLimbo.pop(packet.sourceIP, [])   #pop original messages/links from message limbo
                        for job in originalJobs:                                    #send original messages on original links
                            self.prepareMessage(job[0], job[1])
            elif message.payload.protocolType == "RoutingTable":
                consoleRec("ROUTER " + self.name + " received a RoutingTable-type message")
                TableData = message.payload.payload.payload
                for guestEntry in TableData.itervalues():
                    if ((guestEntry[0] in self.routingTable) and (guestEntry[3]+1 < self.routingTable[guestEntry[0]][3])) or not (guestEntry[0] in self.routingTable):              #if better route, or new route...
                        self.routingTable[guestEntry[0]] = [guestEntry[0], guestEntry[1], simID_link, guestEntry[3]+1]                                                              #update/add to table
            elif message.protocolType == "default":
                consoleRec("ROUTER " + self.name + " received a default-type message")
                packet = message.payload
                consoleRec("ROUTER " + self.name + " routing message...")
                bestRout = self.findRoute(packet)
                if bestRout is None:
                    consoleRec("no routing info OR default gateway for this router.  packet dropped")
                else:
                    if simID_link in self.link_NAT:
                        self.prepareMessage(self.link_NAT[simID_link].postNAT(message), bestRout)
                    else:
                        self.prepareMessage(packet, bestRout)                                              #forward/rout message


    def respondToArp(self, destinationIP, destinationMAC, sourceIP, sourceMAC, segment, simID_link):
        message = ArpResponseTEST(sourceIP, destinationIP, sourceMAC, destinationMAC, segment)
        consoleRec("ROUTER " + self.name + " Constructed ARP response")
        self.outputOntoLink(message, simID_link)


    def findRoute(self, packet):                                                                    #returns output interface of the next hop
        for entry in self.routingTable.itervalues():
            if self.check_isDestinationInSubnet(packet.destinationIP, entry[0], entry[1]):          #look for routing info
                return entry[2]
        if self.defaultGateway in [i[1] for i in self.link_IP.itervalues()]:                        #no routing info found
            consoleRec("no routing info for destination.  sending to default gateway")              #default rout taken
            return self.IP_Link[self.defaultGateway]
        else:
            return None                                                                             #no default gateway


    def check_isDestinationInSubnet(self, destinationIP, thisIP, subnetMask):
        destinationIP_Bytes = [int(x) for x in destinationIP.split(".")]
        thisIP_Bytes = [int(x) for x in thisIP.split(".")]
        subnetMask_Bytes = [int(x) for x in subnetMask.split(".")]
        for i in range(0, len(destinationIP_Bytes)):
            if not ((destinationIP_Bytes[i] & subnetMask_Bytes[i]) is (thisIP_Bytes[i] & subnetMask_Bytes[i])):
                return False
        return(True)


    def prepareMessage(self, packet, link):
        macKeyIP = ""
        if (self.link_IP[link][1] is None):
            if link in self.link_NAT:
                self.outputOntoLink(self.link_NAT[link].resolveNAT(packet), link)     #private router or private destination.  start with private destination
                return None                                                           #break
            else:
                macKeyIP = packet.destinationIP
        else:
            macKeyIP = self.link_IP[link][1]                                          #lookup MAC Address for next hop, or final destination(LAN)
        if macKeyIP in self.lookupTable_MAC:
            destinationMAC = self.lookupTable_MAC[macKeyIP]                                               #Send packet do Mac Address
            message = Frame(self.macAddress, destinationMAC, packet)
            self.outputOntoLink(message, link)
        else:                                                                                             #If MAC not in lookup table, send out ARP and message -> limbo
            consoleRec("ROUTER " + self.name + " no mac-address stored for destinationIP, Preparing ARP")
            self.sendArp(macKeyIP, link, packet)


    def sendArp(self, destinationIP, link, originalPacket=None):                                                 #send ARP message and add original packet to messageLimbo
        arpMessage = ArpRequestTEST(self.link_IP[link][0], destinationIP, self.macAddress)
        if not(originalPacket is None):
            consoleRec("adding parent message to messageLimbo")
            if destinationIP in self.messageLimbo:
                self.messageLimbo[destinationIP].append([originalPacket, link])
            else:
                self.messageLimbo[destinationIP] = [[originalPacket, link]]
        consoleRec("Sending ARP")
        self.outputOntoLink(arpMessage, link)


    def shareTable(self):                                                             #for each router-to-router connection
        for point_IPs in self.link_IP.itervalues():
            if not point_IPs[1] is None:
                packet = Packet(point_IPs[0], point_IPs[1], Segment(0,0,self.routingTable), protocolType="RoutingTable")
                self.prepareMessage(packet, self.IP_Link[point_IPs[0]])


#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#objects of the classes below may appear (possibly with multiple instances) inside of certain Node types
# -PAT
# -Transport
# -ClientApplication
# -ServerApplication

class PAT(object):
    #PAT class contains dynamic mappings for PAT interfaces inside routers.
    #instanciated PAT objects will be inside the "Link_NAT" dictionary inside its respactive router
    def __init__(self, privateIP, publicIP, simID_router):
        self.publicIP = publicIP
        self.privateIP = privateIP
        self.simID_router = simID_router
        self.lookupTable_MAC = {}
        self.NAT_Table = {}                                                             # "newIP:newPort"  -->  [sourceIP, sourcePort]
        self.portKeyInit = 2000                                                         #arbitrary starting port number.  currently 2000+
        IPSet.add(self.privateIP)

    def postNAT(self, frame):
        packet = frame.payload
        self.lookupTable_MAC[packet.sourceIP] = frame.sourceMAC
        #-------------------------
        newIP = self.publicIP
        newPort = self.getNewPort([packet.sourceIP, packet.payload.sourcePort])
        print(packet.sourceIP)
        newPacket = self.buildAliasPacket(packet, newIP, newPort)
        print(packet.sourceIP)
        consoleRec("logging PAT: " +  packet.sourceIP + ":" + packet.payload.sourcePort + "-->" + self.publicIP+":"+ newPort)
        self.NAT_Table[newIP+":"+newPort] = [packet.sourceIP, packet.payload.sourcePort]
        return newPacket

    def resolveNAT(self, packet):
        consoleRec("resolving PAT: " + packet.destinationIP+":"+packet.payload.destinationPort + " --> " + str(self.NAT_Table[packet.destinationIP+":"+packet.payload.destinationPort]))
        resolvedIP = self.NAT_Table[packet.destinationIP+":"+packet.payload.destinationPort][0]
        resolvedPort = self.NAT_Table[packet.destinationIP+":"+packet.payload.destinationPort][1]
        resolvedPacket = self.buildResolvedPacket(packet, resolvedIP, resolvedPort)
        return Frame(nodeDict[self.simID_router].macAddress, self.lookupTable_MAC[resolvedIP], resolvedPacket)

    #allocate unique ports
    def getNewPort(self, privateIP_and_Port):
        portValCheckIter = self.portKeyInit
        while (str(self.publicIP+":"+str(portValCheckIter)) in self.NAT_Table and not(self.NAT_Table[str(self.publicIP+":"+str(portValCheckIter))] == privateIP_and_Port)) :
            print(str(str(self.publicIP+":"+str(portValCheckIter)) in self.NAT_Table) + " " + str(not(self.NAT_Table[str(self.publicIP+":"+str(portValCheckIter))] == privateIP_and_Port)))
            portValCheckIter += 1
        return str(portValCheckIter)

    def buildAliasPacket(self, oldPacket, newIP, newPort):
        result = copy.deepcopy(oldPacket)
        result.sourceIP = newIP
        result.payload.sourcePort = newPort
        return result

    def buildResolvedPacket(self, oldPacket, newIP, newPort):
        result = copy.deepcopy(oldPacket)
        result.destinationIP = newIP
        result.payload.destinationPort = newPort
        return result


class Transport(object):
    def __init__(self, clientSimID):
        self.connections = {}               # socket ->  [windowSize, maxSegSize, seqNum, ackNum, sourceIP, destinationIP, sourcePort, destinationPort, inBuff]
        self.clientSimID = clientSimID
        self.timeout_resend = {}            # socket + ackNum -> [timeLeft, message] TODO: this
        self.outBuff = {}                   # socket -> data
        self.inBuff = {}                    # socket -> data

    #TODO: refactor transport.  messages made in ifs, not built over multiple controll flows

    def packageData(self, dataBytes, connection):
        maxSegSize = self.connections[connection][1]
        segStart = 0
        sourceIP = self.connections[connection][4]
        destinationIP = self.connections[connection][5]
        sourcePort = self.connections[connection][6]
        destinationPort = self.connections[connection][7]
        while segStart < len(dataBytes):                                                                                                  #TODO: sequence numbers should be mod 2*32,  as they should eventually cycle
            data = dataBytes[segStart:min(segStart+maxSegSize, len(dataBytes))]
            message = Packet(sourceIP, destinationIP, TCPSeg(sourcePort, destinationPort, self.connections[connection][3], self.connections[connection][2], payload=Data(data)), protocolType="TCP")
            segStart += maxSegSize
            self.connections[connection][2] += len(data)
            self.transportOut(message)
            self.timeout_resend[connection + "-" + str(self.connections[connection][2])] = [stepCount[0]+20, message]                      #resend if no ack after 50 steps


    def transportOut(self, message):
        link = next(iter(nodeDict[self.clientSimID].linkSet))
        nodeDict[self.clientSimID].prepareMessage(message, link)

    def handle(self, packet):
        segment = packet.payload
        socket = [packet.sourceIP, segment.sourcePort]
        response = Packet(packet.destinationIP, packet.sourceIP, TCPSeg(segment.destinationPort, segment.sourcePort, segment.acknowledgmentNum, segment.sequenceNum), protocolType="TCP")
        #check, then...

        if segment.ACK:
            if not(str(socket[0])+":"+str(socket[1]) in self.connections):
                if not(segment.SYN):
                    consoleRec("PACKET received(ACK) --- for " + str(socket[0])+":"+str(socket[1]) + "-" + str(segment.acknowledgmentNum), tag="transport")
                else:
                    consoleRec("PACKET Received(SYN-ACK) ... sending ACK: --- " + str(socket[0])+":"+str(socket[1]) + "-" + str(segment.sequenceNum), tag="transport")
                self.connections[str(socket[0])+":"+str(socket[1])] = [segment.windowSize, segment.maxSegSize, 1, 1, packet.destinationIP, packet.sourceIP, segment.destinationPort, segment.sourcePort, {}]                          #maybe store seq number here? and windowSize
                nodeDict[self.clientSimID].applications[segment.destinationPort].connectionEstablished(str(socket[0])+":"+str(socket[1]))
            else:
                consoleRec("PACKET received(ACK) --- for " + str(socket[0])+":"+str(socket[1]) + "-" + str(segment.acknowledgmentNum) + "  ---  removing mesage from 'timeout_resend' queue", tag="transport")
                self.timeout_resend.pop(str(socket[0])+":"+str(socket[1]) + "-" + str(segment.acknowledgmentNum), None)
                                                                                                                        #TODO: get rid of this socket rubbish.  necessary refactor!!!!!!!!!!!!!!!!!!!!
        if segment.SYN:
            if not(segment.ACK):
                response.payload.SYN = True
                consoleRec("PACKET Received(SYN) ... sending SYN-ACK: --- " + str(socket[0])+":"+str(socket[1]) + "-" + str(segment.sequenceNum), tag="transport")
            response.payload.ACK = True
            response.payload.acknowledgmentNum = response.payload.acknowledgmentNum+1
            response.payload.sequenceNum = 0
            response.payload.maxSegSize = 10
            response.payload.windowSize = 1000
            # consoleRec("Packet Received(SYN) --- SYN: " + str(socket[0])+":"+str(socket[1]) + "-" + str(segment.sequenceNum + len(segment.payload.data)), tag="transport")
            self.transportOut(response)
                                                                                                                        #TODO: FIN
        #data
        if not(segment.SYN) and not(segment.ACK) and not(segment.FIN) :
            if not(str(socket[0])+":"+str(socket[1]) in self.connections):
                consoleRec("PACKET DROPPED (cause: no such connection)  ---" + str(socket[0])+":"+str(socket[1]) + "-" + str(segment.sequenceNum + len(segment.payload.data)), tag="transport")
            else:
                response.payload.ACK = True
                response.payload.acknowledgmentNum = segment.sequenceNum + len(segment.payload.data)
                response.payload.sequenceNum = self.connections[str(socket[0])+":"+str(socket[1])][2]
                response.payload.maxSegSize = 10
                response.payload.windowSize = 1000
                if segment.sequenceNum >= self.connections[str(socket[0])+":"+str(socket[1])][3] and not(str(segment.sequenceNum) in self.connections[str(socket[0])+":"+str(socket[1])][8]):
                    if random.random() > packetDropRate:
                        consoleRec("PACKET Received ... sending ACK:   ---" + str(socket[0])+":"+str(socket[1]) + "-" + str(segment.sequenceNum + len(segment.payload.data)), tag="transport")
                        self.transportOut(response)
                        self.connections[str(socket[0])+":"+str(socket[1])][8][str(segment.sequenceNum)] = segment.payload.data
                    else:
                        consoleRec("PACKET DROPPED (cause: drop rate)  ---" + str(socket[0])+":"+str(socket[1]) + "-" + str(segment.sequenceNum + len(segment.payload.data)), tag="transport")

    #step ops
    def resend_noAck_messages(self):
        for element in self.timeout_resend:
            if stepCount[0] >= self.timeout_resend[element][0]:
                self.transportOut(self.timeout_resend[element][1])        #resend message
                self.timeout_resend[element][0] += 20

    def pokeBuffers(self):                                                #send in-order data to application from buffer
        for connection in self.connections:
            while str(self.connections[connection][3]) in self.connections[connection][8]:
                nodeDict[self.clientSimID].applications[self.connections[connection][6]].parseInputData(self.connections[connection][8][str(self.connections[connection][3])])               #pass ordered buffer data to application
                self.connections[connection][3] = self.connections[connection][3] + len(self.connections[connection][8].pop(str(self.connections[connection][3])))

#-------------------------
#applicaions

class Application(object):
    def __init__(self, clientSimID, appType, data, portNumber):
        self.type = appType
        self.data = data
        self.clientSimID = clientSimID
        self.portNumber = portNumber

        def command(self):
            print("method not implimented for this parent Application")

        def processInputData(self):
            print("method not implimented for this parent Application")


class ClientApplication(Application):
    def __init__(self, clientSimID, appType="client", data=None, portNumber=1500):
        Application.__init__(self, clientSimID, appType, data, portNumber)
        self.state = "ready"               #options:ready, receiving_file
        self.files = {}                    #fileName -> [fileName, size, data]
        self.currentFileBuffer = []        # [fileName, size, data]

    def parseInputData(self, data):
        consoleRec("parsing data arrived at application port", tag="apps")
        parsingIndex = 0
        while parsingIndex < len(data):
            if self.state == "ready":
                self.setState(data[parsingIndex])
                parsingIndex += 1
            elif self.state == "receiving_file":
                if len(self.currentFileBuffer) is 0:
                    self.currentFileBuffer = ["file"+str(len(self.files)), 0, []]
                    self.currentFileBuffer[1] = int(bytes(bytearray(data[parsingIndex:parsingIndex+4])).encode('hex'), 16)
                    parsingIndex += 4
                    consoleRec(self.currentFileBuffer[0] + " download initiated", tag="apps")
                else:
                    unparsedFileBytesLeft = self.currentFileBuffer[1] - len(self.currentFileBuffer[2])
                    fromByteIndex = parsingIndex
                    endByteIndex = min(parsingIndex + unparsedFileBytesLeft, len(data))
                    self.currentFileBuffer[2].extend(data[fromByteIndex:endByteIndex])
                    parsingIndex = endByteIndex
                    if len(self.currentFileBuffer[2]) >= self.currentFileBuffer[1]-1:
                        consoleRec(self.currentFileBuffer[0] + " download completed", tag="apps")
                        self.setState(0)
                        self.files[self.currentFileBuffer[0]] = self.currentFileBuffer
                        self.currentFileBuffer = []


    def setState(self, stateByte):
        if stateByte is 0:
            self.state = "ready"
        elif stateByte is 1:
            self.state = "receiving_file"
        else:
            print("error: unrecognized state --- " + str(stateByte))
        consoleRec(self.clientSimID + " client app " + "set state: " + self.state, tag="apps")


    def connectionEstablished(self, connection):
        consoleRec("connection established!!!", tag="apps")

    def getImage(self, itemName="default"):
        nodeDict[self.clientSimID].transport[self.portNumber]


class ServerApplication(Application):
    def __init__(self, clientSimID, appType="server", data=None, portNumber=2500):
        Application.__init__(self, clientSimID, appType, data, portNumber)


    def processInputData(self):
        print("method not implimented for this parent Application")

    def connectionEstablished(self, connectionKey):
        consoleRec("connection established!!!", tag="apps")
        connection =  nodeDict[self.clientSimID].transport.connections[connectionKey]
        testData = MyApplicationDataProtocol(1, bytearray("aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffffgggggggggghhhhhhhhhhiiiiiiiiiijjjjjjjjjjkkkkkkkkkkllllllllllmmmmmmmmmmnnnnnnnnnnnoooooooooopppppppppp")).encodeBytes()
        connectedMessage = Packet(connection[4], connection[5], TCPSeg(connection[6], connection[7], connection[3], connection[2], payload=Data(testData)), protocolType="TCP")
        nodeDict[self.clientSimID].transport.packageData(testData, connectionKey)
        consoleRec("connection data response sent", tag="apps")


    def handleMessage(self, socket, data):
            print("")

#-------------------------
#custom application protocol

class MyApplicationDataProtocol(object):
    #byte1:        byte2:           byte3:            byte4             byte5         byte6...
    #file(1)     length(bytes) --------------------------------------------------|    file
    #....(2)
    #....(3)
    #....(4)
    #....more flag/commands can be added

    def __init__(self, command, data=None):
        self.command = command
        self.data = data

    #return byteArray representation of this object (to be transmitted)
    def encodeBytes(self):
        result = bytearray()
        result.extend(bytes(bytearray([self.command])))
        if self.command is 1:
            result.extend(bytes(bytearray([(0xff00000000 & len(self.data)) >> 24, (0x00ff0000 & len(self.data)) >> 16, (0x0000ff00 & len(self.data)) >> 8, 0x00ff & len(self.data)])))
            result.extend(self.data)
        return result


#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#message "capsules"
#Frame(Paket(Segment(Data)))

class Data(object):
    def __init__(self, data=None):
        self.data = data


class Segment(object):
    def __init__(self, sourcePort, destinationPort, payload=None):
        self.sourcePort = sourcePort
        self.destinationPort = destinationPort
        self.payload = payload

    def __str__(self):
        return self.getInfo()

    def getInfo(self, pad=0):
        return " " * pad + "Segment(srcPort: " + str(self.sourcePort) + " destPort: " + str(self.sourcePort) + ")"


class Packet(object):
    def __init__(self, sourceIP, destinationIP, payload=None, protocolType="default"):
        self.protocolType = protocolType
        self.sourceIP = sourceIP
        self.destinationIP = destinationIP
        self.payload = payload

    def __str__(self):
        return self.getInfo()

    def getInfo(self, pad=0):
        return " " * pad + "Packet(srcIP: " + str(self.sourceIP) + " destIP: " + str(self.destinationIP) + "\n" + self.payload.getInfo(pad+7) + ")"


class Frame(object):
    def __init__(self, sourceMAC, destinationMAC, payload, protocolType="default"):
        self.sourceMAC = sourceMAC
        self.destinationMAC = destinationMAC
        self.payload = payload
        self.protocolType = protocolType

    def __str__(self):
        return self.getInfo()

    def getInfo(self, pad=0):
        return " " * pad + "Frame(srcMAC: " + str(self.sourceMAC) + " destMAC: " + str(self.destinationMAC) + "\n" + self.payload.getInfo(pad+7) + ")"

#-------------------------
#message "type" prototypes

class ArpRequestTEST(Frame):
    def __init__(self, sourceIP, destinationIP, sourceMAC):
        segmentFiller = Segment(None, None, None)
        packet = Packet(sourceIP, destinationIP, segmentFiller)
        ARP_MAC = "FF-FF-FF-FF-FF-FF"
        Frame.__init__(self, sourceMAC, ARP_MAC, packet, "ArpRequest")


class ArpResponseTEST(Frame):
    def __init__(self, sourceIP, destinationIP, sourceMAC, destinationMAC, segment):
        segmentFiller_Return = segment
        packet = Packet(sourceIP, destinationIP, segmentFiller_Return)
        Frame.__init__(self, sourceMAC, destinationMAC, packet, "ArpResponse")


class RoutingTableTEST(Packet):
    def __init__(self, sourceIP, destinationIP, segment, protocolType="RoutingTable"):
        Packet.__init__(self, sourceIP, destinationIP, segment, protocolType)


class GetResponse(Packet):
    def __init__(self, sourceIP, destinationIP, segment):
        Packet.__init__(self, sourceIP, destinationIP, segment, "get_resp")


class TCPSeg(Segment):
    def __init__(self, sourcePort, destinationPort, acknowledgmentNum, sequenceNum, payload=None, SYN=False, ACK=False, FIN=False, windowSize=1000, maxSegSize=100):
        Segment.__init__(self, sourcePort, destinationPort, payload)
        self.acknowledgmentNum = acknowledgmentNum
        self.sequenceNum = sequenceNum
        self.SYN = SYN
        self.ACK = ACK
        self.FIN = FIN
        self.windowSize = windowSize
        self.maxSegSize = maxSegSize



########################################################################################################################################
########################################################################################################################################
#controller
########################################################################################################################################
########################################################################################################################################


#-----stepping (time)-----
def step():
    consoleOut.append([])
    consoleRec('---------------------------------------------', tag="steps")
    consoleRec("     step " + str(stepCount[0]) + " - transferring link data", tag="steps")
    consoleRec('---------------------------------------------', tag="steps")
    for link in links:
        links[link].transfer()                                            #transfers signals down a link
    for node in nodeDict:
        nodeDict[node].recieveMessage()                                   #nodes read messages on link
        if (nodeDict[node].type is "ROUTER" and (stepCount[0] < 10)):
            nodeDict[node].shareTable()                                   #share routing tables
        if (nodeDict[node].type is "CLIENT"):
            nodeDict[node].transport.pokeBuffers()                        #offload transport layer buffers
            nodeDict[node].transport.resend_noAck_messages()              #transp layer resend messages after timeout
    stepCount[0] = stepCount[0] + 1


#----------view-----------
def printEnv():
    consoleRec('printing environment...')
    consoleRec('NODES:')
    for i in nodeDict:
        consoleRec(str(nodeDict[i]))
        consoleRec("")
    consoleRec('------------------------------------------------------------------------------------------------')
    consoleRec('Links:')
    for i in links:
        consoleRec(i + ":\t" + str(links[i]))
    consoleRec('------------------------------------------------------------------------------------------------')


def consoleRec(inputText, tag="default"):
    consoleOut[stepCount[0]].append([inputText, tag])


def reset():
    global nodeDict
    global links
    global addressPrefixSpace
    global networks
    global stepCount
    global stepHistory
    global consoleOut
    global IPSet

    nodeDict = {}
    links = {}
    addressPrefixSpace = {}
    networks = {}
    stepCount = [0]
    stepHistory = {}                         #step -------> [nodeDict,links,environment]
    consoleOut = [[]]
    IPSet = set()

#----------helpers---------
def isPrivateIPAddress(IPAddress):
        if Router(None, None, None).check_isDestinationInSubnet(IPAddress, "10.0.0.0", "255.0.0.0"):
            return True
        if Router(None, None, None).check_isDestinationInSubnet(IPAddress, "192.168.0.0", "255.255.0.0"):
            return True
        if Router(None, None, None).check_isDestinationInSubnet(IPAddress, "172.16.0.0", "255.240.0.0"):
            return True
        return False


#---------commands---------
def createLink(simID_Device1, simID_Device2, IPAddress_Router1=None, IPAddress_Router2=None, subnetMask=None, linkType=None):
    #info:
    #simID_Device1                           - first device (THIS paramter is router ID for router-basic)
    #simID_Device2                           - second device
    #IPAddress_Router1                       - (specify the IP address of the interface(link) that will be added)
    #IPAddress_Router2                       - ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    #subnetMask                              - subnet mask of the LAN for a router-basic link
    #linkType                                - the type of link (what type of nodes are being connected) --- more info below
    #---
    #accepted linkType formats:
    #type:
    #basic-basic:                            createLink(simID_basic, simID_basic, type=basic-basic)
    #router-basic                            createLink(simID_router, simID_basic, IPAddress_Router1="100.100.100.0", subnetMask="255.255.255.0", linkType="router-basic")
    #router-router                           createLink(simID_router, simID_router, IPAddress_Router1="100.500.200.200", IPAddress_Router2="100.600.200.200", linkType="router-router")


    if linkType == "basic-basic":
        newLink = Link(simID_Device1, simID_Device2)
        links[newLink.simID] = newLink
        nodeDict[simID_Device1].linkSet.add(newLink.simID)
        nodeDict[simID_Device2].linkSet.add(newLink.simID)
        consoleRec('created Basic-Basic link simID: ' + newLink.simID)
        return newLink.simID

    elif linkType == "router-basic":
        IPAddressRouter_Bytes = [int(x) for x in IPAddress_Router1.split(".")]
        subnetMask_Bytes = [int(x) for x in subnetMask.split(".")]
        networkID = ".".join(str(a) for a in [x & y for x, y in zip(IPAddressRouter_Bytes, subnetMask_Bytes)])
        newLink = Link(simID_Device1, simID_Device2)
        links[newLink.simID] = newLink
        nodeDict[simID_Device1].linkSet.add(newLink.simID)
        nodeDict[simID_Device2].linkSet.add(newLink.simID)
        nodeDict[simID_Device1].link_IP[newLink.simID] = [IPAddress_Router1, None]
        nodeDict[simID_Device1].IP_Link[IPAddress_Router1] = newLink.simID
        nodeDict[simID_Device1].routingTable[networkID] = [networkID, subnetMask, newLink.simID, 0]
        consoleRec('created Router-Basic link simID: ' + newLink.simID)
        IPSet.add(IPAddress_Router1)
        return newLink.simID

    elif linkType == "router-router":
        newLink = Link(simID_Device1, simID_Device2)
        links[newLink.simID] = newLink
        nodeDict[simID_Device1].linkSet.add(newLink.simID)
        nodeDict[simID_Device1].link_IP[newLink.simID] = [IPAddress_Router1, IPAddress_Router2]
        nodeDict[simID_Device1].IP_Link[IPAddress_Router1] = newLink.simID
        nodeDict[simID_Device2].link_IP[newLink.simID] = [IPAddress_Router2, IPAddress_Router1]
        nodeDict[simID_Device2].IP_Link[IPAddress_Router2] = newLink.simID
        nodeDict[simID_Device2].linkSet.add(newLink.simID)
        consoleRec('created Router-Router link simID: ' + newLink.simID)
        IPSet.add(IPAddress_Router1)
        IPSet.add(IPAddress_Router2)
        return newLink.simID


def removeLink(simID_link):
    link = links.pop(simID_link)
    nodeDict[link.simID_DeviceA].linkSet.remove(simID_link)
    nodeDict[link.simID_DeviceB].linkSet.remove(simID_link)
    consoleRec('removed link simID: ' + simID_link)
    return link


def createSwitch(name="unnamed"):
    newSwitch = Switch(name)
    consoleRec('created switch simID: ' + newSwitch.simID)
    nodeDict[newSwitch.simID] = newSwitch
    return newSwitch.simID


def createHub(name="unnamed"):
    newHub = Hub(name)
    consoleRec('created hub simID: ' + newHub.simID)
    nodeDict[newHub.simID] = newHub
    return newHub.simID


def createClient(macAddress, IPAddress, subnetMask, defaultGateway="0.0.0.0", name="a_client"):
    newClient = Client(macAddress, IPAddress, subnetMask, defaultGateway, name)
    consoleRec('created client simID: ' + newClient.simID)
    nodeDict[newClient.simID] = newClient
    IPSet.add(IPAddress)
    return newClient.simID


def createRouter(macAddress, defaultGateway, name="unnamed"):
    newRouter = Router(macAddress, defaultGateway, name)
    consoleRec('created Router simID: ' + newRouter.simID)
    nodeDict[newRouter.simID] = newRouter
    return newRouter.simID


def createNetwork(prefix, size, publicAlias=None):
    rows = []
    if publicAlias is None:
        if not (prefix in addressPrefixSpace):
            addressPrefixSpace[prefix] = [0 for i in range(15)]
        rows = addressPrefixSpace[prefix]
    else:
            rows = [0 for i in range(15)]
    startLoc=rows[size]
    findModList = rows[size:len(rows)]
    modRow = 1000                           #any number larger than the number of rows
    newLoc = None
    for i in range(0, len(findModList)):
        if findModList[i] > rows[size]:
            modRow = min(modRow, i)
    modRow = modRow+size
    if (size < len(rows) and (not(modRow >= 1000) and ((rows[size] + 2**(size+2)) % (2**(modRow+2)) is 0))):
        newLoc = rows[size+1]
    else:
        newLoc = rows[size] + 2**(size+2)

    for i in range(0, len(rows)):
        index = rows[i]
        if i > size and rows[i] is rows[size]:
            rows[i] = rows[i] + 2**(i+2)
            if i < len(rows)-1:
                rows[i] = max(rows[i], rows[i+1])
        if i < size and rows[i] is rows[size]:
            rows[i] = newLoc
    rows[size] = newLoc
    networkID = prefix + "." + str((startLoc & (0x0000 | 0xff00)) >> 8) + "." + str(startLoc & (0x00 | 0xff))
    subnetMask = "255.255" + "." + str(((2**(size+2))-1 ^ (0x0000 | 0xff00)) >> 8) + "." + str(((2**(size+2))-1 ^ (0x00 | 0xff)) & 0x00ff)
    broadcastAddress = prefix + "." + str(((startLoc+(2**(size+2))) & (0x0000 | 0xff00)) >> 8) + "." + str(int((startLoc+(2**(size+2)))-1 & (0x00 | 0xff)))
    hostIPs = [str(prefix + "." + str((net & (0x0000 | 0xff00)) >> 8) + "." + str(net & (0x00 | 0xff))) for net in range(startLoc+1,startLoc + 2**(size+2)-1)]
    if publicAlias is None:
        networks[networkID] = Network(networkID, subnetMask, broadcastAddress, hostIPs)
    else:
        networks[publicAlias] = Network(networkID, subnetMask, broadcastAddress, hostIPs)
    return Network(networkID, subnetMask, broadcastAddress, hostIPs)


def initiateMessage(fromSimID, toSimID, fromPort, toPort, messageType):
    consoleRec("Initiating message: from " + nodeDict[fromSimID].IPAddress + " to " + nodeDict[toSimID].IPAddress)
    message = Packet(nodeDict[fromSimID].IPAddress, nodeDict[toSimID].IPAddress, Segment(fromPort,toPort), messageType)
    link = next(iter(nodeDict[fromSimID].linkSet))
    nodeDict[fromSimID].prepareMessage(message, link)


def initiateMessageTest(fromSimID, toSimID, fromPort, toPort, messageType):
    consoleRec("Initiating message: from " + nodeDict[fromSimID].IPAddress + " to " + nodeDict[toSimID].IPAddress)
    link = next(iter(nodeDict[fromSimID].linkSet))
    if messageType is "TCP":
        message = Packet(nodeDict[fromSimID].IPAddress, nodeDict[toSimID].IPAddress, TCPSeg(fromPort, toPort, 0, 0, SYN=True, payload=Data()), protocolType="TCP")
        nodeDict[fromSimID].transport.transportOut(message)
    else:
        message = Packet(nodeDict[fromSimID].IPAddress, nodeDict[toSimID].IPAddress, Segment(fromPort,toPort), messageType)
        nodeDict[fromSimID].prepareMessage(message, link)














####################################################################################################################################################
####################################################################################################################################################
consoleRec("sim init")
